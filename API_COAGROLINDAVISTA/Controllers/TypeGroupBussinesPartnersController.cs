﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Services.interfaces;

namespace API_COAGROLINDAVISTA.Controllers
{
    public class TypeGroupBussinesPartnersController : ApiController
    {
        #region Variables in the scope TypeBussinesPartnersController
        ITypeGroupBussinesPartnersService TypeBussinesPartnersService;
        #endregion
        #region Constructor DI Entities
        public TypeGroupBussinesPartnersController(ITypeGroupBussinesPartnersService TypeBussinesPartnersService)
        {
            this.TypeBussinesPartnersService = TypeBussinesPartnersService;
        }
        #endregion
        #region GET
        #region ENDPOINT GET: api/TypeBussinesPartners
        [HttpGet]
        public IQueryable<OTGB> GetOTGB()
        {
            return TypeBussinesPartnersService.GetOTGB();
        }
        #endregion
        #region ENDPOINT GET: api/TypeBussinesPartners/{id}
        [HttpGet]
        [ResponseType(typeof(OTGB))]
        public IHttpActionResult GetOTGB(int id)
        {
            var response = TypeBussinesPartnersService.GetOTGBById(id);
            if (response == null)
            {
                return NotFound();
            }
            return Ok(response);
        }
        #endregion
        #endregion
        #region POST
        #region POST: api/TypeBussinesPartners
        // POST: api/TypeBussinesPartners
        [HttpPost]
        [ResponseType(typeof(OTGB))]
        public IHttpActionResult PostOTGB(OTGB OTGB)
        {
            var response = TypeBussinesPartnersService.PostOTGB(OTGB);
            if (response.Equals(StatusServices.StatusService.Error.ToString()))
            {
                return BadRequest();
            }
            return Ok();
        }
        #endregion
        #endregion
        #region PUT
        #region PUT: api/TypeBussinesPartners/5
        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutOTGB(OTGB OTGB)
        {
            var Response = TypeBussinesPartnersService.PutOTGB(OTGB);
            if (Response.Equals(StatusServices.StatusService.Succes.ToString()))
            {
                return Ok(StatusServices.StatusService.Succes.ToString());
            }
            return BadRequest(Response.ToString());
        }
        #endregion
        #endregion
        #region DELETE
        #region DELETE: api/TypeBussinesPartners/5
        [HttpDelete]
        [ResponseType(typeof(OTGB))]
        public IHttpActionResult DeleteOTGB(int id)
        {
            var Response = TypeBussinesPartnersService.DeleteOTGB(id);
            if (Response.Equals(StatusServices.StatusService.Succes.ToString()))
            {
                return Ok(StatusServices.StatusService.Succes.ToString());
            }
            return BadRequest(StatusServices.StatusService.Error.ToString());
        }

        #endregion
        #endregion
        #region DIPOSE
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                TypeBussinesPartnersService.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}