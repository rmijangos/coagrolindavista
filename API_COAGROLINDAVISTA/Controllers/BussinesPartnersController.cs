﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Services.interfaces;

namespace API_COAGROLINDAVISTA.Controllers
{
    public class BussinesPartnersController : ApiController
    {
        #region Variables in the scope BussinesPartnersController
        IBussinesPartnersService BussinesPartnersService;
        #endregion
        #region Constructor DI Entities
        public BussinesPartnersController(IBussinesPartnersService BussinesPartnersService)
        {
            this.BussinesPartnersService = BussinesPartnersService;
        }
        #endregion
        #region GET
        #region ENDPOINT GET: api/BussinesPartners
        [HttpGet]
        public IQueryable<OBPR> GetOBPR()
        {
            return BussinesPartnersService.GetOBPR();
        }
        #endregion
        #region ENDPOINT GET: api/BussinesPartners/{id}
        [HttpGet]
        [ResponseType(typeof(OBPR))]
        public IHttpActionResult GetOBPR(String id)
        {
            var response = BussinesPartnersService.GetOBPRById(id);
            if (response == null)
            {
                return NotFound();
            }
            return Ok(response);
        }
        #endregion
        #endregion
        #region POST
        #region POST: api/BussinesPartners
        // POST: api/BussinesPartners
        [HttpPost]
        [ResponseType(typeof(OBPR))]
        public IHttpActionResult PostOBPR(OBPR OBPR)
        {
            var response = BussinesPartnersService.PostOBPR(OBPR);
            if (response.Equals(StatusServices.StatusService.Error.ToString()))
            {
                return BadRequest();
            }
            return Ok();
        }
        #endregion
        #endregion
        #region PUT
        #region PUT: api/BussinesPartners/5
        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutOBPR(OBPR OBPR)
        {
            var Response = BussinesPartnersService.PutOBPR(OBPR);
            if (Response.Equals(StatusServices.StatusService.Succes.ToString()))
            {
                return Ok(StatusServices.StatusService.Succes.ToString());
            }
            return BadRequest(Response.ToString());
        }
        #endregion
        #endregion
        #region DELETE
        #region DELETE: api/BussinesPartners/5
        [HttpDelete]
        [ResponseType(typeof(OBPR))]
        public IHttpActionResult DeleteOBPR(int id)
        {
            var Response = BussinesPartnersService.DeleteOBPR(id);
            if (Response.Equals(StatusServices.StatusService.Succes.ToString()))
            {
                return Ok(StatusServices.StatusService.Succes.ToString());
            }
            return BadRequest(StatusServices.StatusService.Error.ToString());
        }

        #endregion
        #endregion
        #region DIPOSE
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                BussinesPartnersService.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}