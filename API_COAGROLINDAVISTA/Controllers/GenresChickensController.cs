﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Services.interfaces;

namespace API_COAGROLINDAVISTA.Controllers
{
    public class GenresChickensController : ApiController
    {
        #region Variables in the scope GenresChickensController
        IGenresChickensService GenreChickenService;
        #endregion
        #region Constructor DI Entities
        public GenresChickensController(IGenresChickensService GenreChickenService)
        {
            this.GenreChickenService = GenreChickenService;
        }
        #endregion
        #region GET
        #region ENDPOINT GET: api/GenresChickens
        [HttpGet]
        public IQueryable<OCHK> GetOCHK()
        {
            return GenreChickenService.GetOCHK();
        }
        #endregion
        #region ENDPOINT GET: api/GenresChickens/{id}
        [HttpGet]
        [ResponseType(typeof(OCHK))]
        public IHttpActionResult GetOCHK(int id)
        {
            var response = GenreChickenService.GetOCHKById(id);
            if (response == null)
            {
                return NotFound();
            }
            return Ok(response);
        }
        #endregion
        #endregion
        #region POST
        #region POST: api/GenresChickens
        // POST: api/GenresChickens
        [HttpPost]
        [ResponseType(typeof(OCHK))]
        public IHttpActionResult PostOCHK(OCHK OCHK)
        {
            var response = GenreChickenService.PostOCHK(OCHK);
            if (response.Equals(StatusServices.StatusService.Error.ToString()))
            {
                return BadRequest();
            }
            return Ok();
        }
        #endregion
        #endregion
        #region PUT
        #region PUT: api/GenresChickens/5
        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutOCHK(OCHK OCHK)
        {
            var Response = GenreChickenService .PutOCHK(OCHK);
            if (Response.Equals(StatusServices.StatusService.Succes.ToString()))
            {
                return Ok(StatusServices.StatusService.Succes.ToString());
            }
            return BadRequest(Response.ToString());
        }
        #endregion
        #endregion
        #region DELETE
        #region DELETE: api/GenresChickens/5
        [HttpDelete]
        [ResponseType(typeof(OCHK))]
        public IHttpActionResult DeleteOCHK(int id)
        {
            var Response = GenreChickenService .DeleteOCHK(id);
            if (Response.Equals(StatusServices.StatusService.Succes.ToString()))
            {
                return Ok(StatusServices.StatusService.Succes.ToString());
            }
            return BadRequest(StatusServices.StatusService.Error.ToString());
        }

        #endregion
        #endregion
        #region DIPOSE
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                GenreChickenService .Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}