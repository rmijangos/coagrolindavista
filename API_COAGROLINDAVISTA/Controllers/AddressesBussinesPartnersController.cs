﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Services.interfaces;

namespace API_COAGROLINDAVISTA.Controllers
{
    public class AddressesBussinesPartnersController : ApiController
    {
        #region Variables in the scope AddressesBussinesPartnersController
        IAddressBussinesPartnerService AddressBussinesPartnerService;
        #endregion
        #region Constructor DI Entities
        public AddressesBussinesPartnersController(IAddressBussinesPartnerService AddressBussinesPartnerService)
        {
            this.AddressBussinesPartnerService = AddressBussinesPartnerService;
        }
        #endregion
        #region GET
        #region ENDPOINT GET: api/AddressesBussinesPartners
        [HttpGet]
        public IQueryable<OBPA> GetOBPA()
        {
            return AddressBussinesPartnerService.GetOBPA();
        }
        #endregion
        #region ENDPOINT GET: api/AddressesBussinesPartners/{id}
        [HttpGet]
        [ResponseType(typeof(OBPA))]
        public IHttpActionResult GetOBPA(int id)
        {
            var response = AddressBussinesPartnerService.GetOBPAById(id);
            if (response == null)
            {
                return NotFound();
            }
            return Ok(response);
        }
        #endregion
        #endregion
        #region POST
        #region POST: api/AddressesBussinesPartners
        // POST: api/AddressesBussinesPartners
        [HttpPost]
        [ResponseType(typeof(OBPA))]
        public IHttpActionResult PostOBPA(OBPA OBPA)
        {
            var response = AddressBussinesPartnerService.PostOBPA(OBPA);
            if (response.Equals(StatusServices.StatusService.Error.ToString()))
            {
                return BadRequest();
            }
            return Ok();
        }
        #endregion
        #endregion
        #region PUT
        #region PUT: api/AddressesBussinesPartners/5
        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutOBPA(OBPA OBPA)
        {
            var Response = AddressBussinesPartnerService.PutOBPA(OBPA);
            if (Response.Equals(StatusServices.StatusService.Succes.ToString()))
            {
                return Ok(StatusServices.StatusService.Succes.ToString());
            }
            return BadRequest(Response.ToString());
        }
        #endregion
        #endregion
        #region DELETE
        #region DELETE: api/AddressesBussinesPartners/5
        [HttpDelete]
        [ResponseType(typeof(OBPA))]
        public IHttpActionResult DeleteOBPA(int id)
        {
            var Response = AddressBussinesPartnerService.DeleteOBPA(id);
            if (Response.Equals(StatusServices.StatusService.Succes.ToString()))
            {
                return Ok(StatusServices.StatusService.Succes.ToString());
            }
            return BadRequest(StatusServices.StatusService.Error.ToString());
        }

        #endregion
        #endregion
        #region DIPOSE
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                AddressBussinesPartnerService.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}