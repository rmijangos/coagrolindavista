﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Services.interfaces;

namespace API_COAGROLINDAVISTA.Controllers
{
    [Authorize]
    public class ProductionsController : ApiController
    {
        #region Variables in the scope SheldsController
        IProductionsService ProductionService;
        #endregion
        #region Constructor DI Entities
        public ProductionsController(IProductionsService ProductionService)
        {
            this.ProductionService = ProductionService;
        }
        #endregion
        #region GET
        #region ENDPOINT GET: api/Productions
        /// <summary>
        /// This Endpoint Returns an overview of productions in the company (Needs authorization JWT)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IQueryable<OPRO> GetOPRO()
        {
            return ProductionService.GetOPRO();
        }
        #endregion

        #region ENDPOINT GET: api/Productions/{id}
        [HttpGet]
        [ResponseType(typeof(OPRO))]
        public IHttpActionResult GetOPRO(int id)
        {
            var response = ProductionService.GetOPROById(id);
            if (response == null)
            {
                return NotFound();
            }
            return Ok(response);
        }
        #endregion
        #endregion
        #region POST
        #region POST: api/Productions
        // POST: api/Shelds
        [HttpPost]
        [ResponseType(typeof(OPRO))]
        public IHttpActionResult PostOPRO(OPRO OPRO)
        {
            var response = ProductionService.PostOPRO(OPRO);
            if (response.Equals(StatusServices.StatusService.Error.ToString()))
            {
                return BadRequest();
            }
            return Ok();
        }
        #endregion
        #endregion
        #region PUT
        #region PUT: api/Productions/5
        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutOPRO(OPRO OPRO)
        {
            var Response = ProductionService.PutOPRO(OPRO);
            if (Response.Equals(StatusServices.StatusService.Succes.ToString()))
            {
                return Ok(StatusServices.StatusService.Succes.ToString());
            }
            return BadRequest(Response.ToString());
        }
        #endregion
        #endregion
        #region DELETE
        #region DELETE: api/Shelds/5
        [HttpDelete]
        [ResponseType(typeof(OPRO))]
        public IHttpActionResult DeleteOPRO(int id)
        {
            var Response = ProductionService.DeleteOPRO(id);
            if (Response.Equals(StatusServices.StatusService.Succes.ToString()))
            {
                return Ok(StatusServices.StatusService.Succes.ToString());
            }
            return BadRequest(StatusServices.StatusService.Error.ToString());
        }

        #endregion
        #endregion
        #region DIPOSE
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                ProductionService.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}