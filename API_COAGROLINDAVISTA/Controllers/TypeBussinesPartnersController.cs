﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Services.interfaces;

namespace API_COAGROLINDAVISTA.Controllers
{
    public class TypeBussinesPartnersController : ApiController
    {
        #region Variables in the scope BussinesPartnersController
        ITypeBussinesPartnerService BussinesPartnerService;
        #endregion
        #region Constructor DI Entities
        public TypeBussinesPartnersController(ITypeBussinesPartnerService BussinesPartnerService)
        {
            this.BussinesPartnerService = BussinesPartnerService;
        }
        #endregion
        #region GET
        #region ENDPOINT GET: api/BussinesPartners
        [HttpGet]
        public IQueryable<OTBP> GetOTBP()
        {
            return BussinesPartnerService.GetOTBP();
        }
        #endregion
        #region ENDPOINT GET: api/BussinesPartners/{id}
        [HttpGet]
        [ResponseType(typeof(OTBP))]
        public IHttpActionResult GetOTBP(int id)
        {
            var response = BussinesPartnerService.GetOTBPById(id);
            if (response == null)
            {
                return NotFound();
            }
            return Ok(response);
        }
        #endregion
        #endregion
        #region POST
        #region POST: api/BussinesPartners
        // POST: api/BussinesPartners
        [HttpPost]
        [ResponseType(typeof(OTBP))]
        public IHttpActionResult PostOTBP(OTBP OTBP)
        {
            var response = BussinesPartnerService.PostOTBP(OTBP);
            if (response.Equals(StatusServices.StatusService.Error.ToString()))
            {
                return BadRequest();
            }
            return Ok();
        }
        #endregion
        #endregion
        #region PUT
        #region PUT: api/BussinesPartners/5
        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutOTBP(OTBP OTBP)
        {
            var Response = BussinesPartnerService.PutOTBP(OTBP);
            if (Response.Equals(StatusServices.StatusService.Succes.ToString()))
            {
                return Ok(StatusServices.StatusService.Succes.ToString());
            }
            return BadRequest(Response.ToString());
        }
        #endregion
        #endregion
        #region DELETE
        #region DELETE: api/BussinesPartners/5
        [HttpDelete]
        [ResponseType(typeof(OTBP))]
        public IHttpActionResult DeleteOTBP(int id)
        {
            var Response = BussinesPartnerService.DeleteOTBP(id);
            if (Response.Equals(StatusServices.StatusService.Succes.ToString()))
            {
                return Ok(StatusServices.StatusService.Succes.ToString());
            }
            return BadRequest(StatusServices.StatusService.Error.ToString());
        }

        #endregion
        #endregion
        #region DIPOSE
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                BussinesPartnerService.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}