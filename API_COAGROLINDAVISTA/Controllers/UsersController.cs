﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Services.interfaces;

namespace API_COAGROLINDAVISTA.Controllers
{
    public class UsersController : ApiController
    {
        #region Variables in the scope UsersController
        IUserService UserService;
        #endregion
        #region Constructor DI Entities
        public UsersController(IUserService UserService)
        {
            this.UserService = UserService;
        }
        #endregion
        #region GET
        #region ENDPOINT GET: api/Users
        [HttpGet]
        public IQueryable<OUSR> GetOUSR()
        {
            return UserService.GetOUSR();
        }
        #endregion
        #region ENDPOINT GET: api/Users/{id}
        [HttpGet]
        [ResponseType(typeof(OUSR))]
        public IHttpActionResult GetOUSR(int id)
        {
            var response = UserService.GetOUSRById(id);
            if (response == null)
            {
                return NotFound();
            }
            return Ok(response);
        }
        #endregion
        #region ENDPOINT GET api/Users/UserLogIn/{User}/{Pass}
        [HttpGet]
        [Route("api/Users/UserLogIn/{User}/{Pass}")]
        public IHttpActionResult UserLogIn(String User, String Pass)
        {
            var response = UserService.GetOUSRByUserAndPass(User, Pass);
            if (response == null)
            {
                return NotFound();
            }
            return Ok(response);
        }
        #endregion
        #endregion
        #region POST
        #region POST: api/Users
        // POST: api/Users
        [HttpPost]
        [ResponseType(typeof(OUSR))]
        public IHttpActionResult PostOUSR(OUSR OUSR)
        {
            var response = UserService.PostOUSR(OUSR);
            if (response.Equals(StatusServices.StatusService.Error.ToString()))
            {
                return BadRequest();
            }
            return Ok();
        }
        #endregion
        #endregion
        #region PUT
        #region PUT: api/Users/5
        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutOUSR(OUSR OUSR)
        {
            var Response = UserService.PutOUSR(OUSR);
            if (Response.Equals(StatusServices.StatusService.Succes.ToString()))
            {
                return Ok(StatusServices.StatusService.Succes.ToString());
            }
            return BadRequest(Response.ToString());
        }
        #endregion
        #endregion
        #region DELETE
        #region DELETE: api/Users/5
        [HttpDelete]
        [ResponseType(typeof(OUSR))]
        public IHttpActionResult DeleteOUSR(int id)
        {
            var Response = UserService.DeleteOUSR(id);
            if (Response.Equals(StatusServices.StatusService.Succes.ToString()))
            {
                return Ok(StatusServices.StatusService.Succes.ToString());
            }
            return BadRequest(StatusServices.StatusService.Error.ToString());
        }

        #endregion
        #endregion
        #region DIPOSE
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                UserService.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}