﻿using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Services.interfaces;

namespace API_COAGROLINDAVISTA.Controllers
{
    public class SheldsController : ApiController
    {
        #region Variables in the scope SheldsController
        IShedService ShedService;
        #endregion
        #region Constructor DI Entities
        public SheldsController(IShedService ShedService)
        {
            this.ShedService = ShedService;
        }
        #endregion
        #region GET
        #region ENDPOINT GET: api/Shelds
        [HttpGet]
        public IQueryable<OSHE> GetOSHE()
        {
            return ShedService.GetOSHE();
        }
        #endregion

        #region ENDPOINT GET: api/Shelds/{id}
        [HttpGet]
        [ResponseType(typeof(OSHE))]
        public IHttpActionResult GetOSHE(int id)
        {
            var response = ShedService.GetOSHEById(id);
            if (response == null)
            {
                return NotFound();
            }
            return Ok(response);
        }
        #endregion
        #endregion
        #region POST
        #region POST: api/Shelds
        // POST: api/Shelds
        [HttpPost]
        [ResponseType(typeof(OSHE))]
        public IHttpActionResult PostOSHE(OSHE oSHE)
        {
            var response = ShedService.PostOSHE(oSHE);
            if (response.Equals(StatusServices.StatusService.Error.ToString()))
            {
                return BadRequest();
            }
            return Ok();
        }
        #endregion
        #endregion
        #region PUT
        #region PUT: api/Shelds/5
        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutOSHE(OSHE Oshe)
        {
            var Response = ShedService.PutOSHE(Oshe);
            if (Response.Equals(StatusServices.StatusService.Succes.ToString()))
            {
                return Ok(StatusServices.StatusService.Succes.ToString());
            }
            return BadRequest(Response.ToString());
        }
        #endregion
        #endregion
        #region DELETE
        #region DELETE: api/Shelds/5
        [HttpDelete]
        [ResponseType(typeof(OSHE))]
        public IHttpActionResult DeleteOSHE(int id)
        {
            var Response = ShedService.DeleteOSHE(id);
            if (Response.Equals(StatusServices.StatusService.Succes.ToString()))
            {
                return Ok(StatusServices.StatusService.Succes.ToString());
            }
            return BadRequest(StatusServices.StatusService.Error.ToString());
        }

        #endregion
        #endregion
        #region DIPOSE
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                ShedService.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}