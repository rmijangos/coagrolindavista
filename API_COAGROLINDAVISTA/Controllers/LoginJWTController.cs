﻿using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Services.interfaces;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace API_COAGROLINDAVISTA.Controllers
{
    public class LoginJWTController : ApiController
    {
        #region Variables in the scope SheldsController
        IGenerateJWTService GenJWTService;
        #endregion
        #region Constructor DI Entities
        public LoginJWTController(IGenerateJWTService GenJWTService)
        {
            this.GenJWTService = GenJWTService;
        }
        #endregion
        #region POST: api/LoginJWT
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult LoginJWT(UserLogin UserLogin)
        {
            if (UserLogin == null)
                return BadRequest("Credentials are required");
            var _userInfo = GenJWTService.AuthUser(UserLogin.User, UserLogin.Password);
            if (_userInfo != null)
            {
                return Ok(new { token = GenJWTService.GenerateToken(_userInfo) });
            }
            else
            {
                return Unauthorized();
            }
        }
        #endregion
    }
}
