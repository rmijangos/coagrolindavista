﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Services.interfaces;

namespace API_COAGROLINDAVISTA.Controllers
{
    public class UsersRolesController : ApiController
    {
        #region Variables in the scope UsersRolesController
        IUserRoleService UserRoleService;
        #endregion
        #region Constructor DI Entities
        public UsersRolesController(IUserRoleService UserRoleService)
        {
            this.UserRoleService = UserRoleService;
        }
        #endregion
        #region GET
        #region ENDPOINT GET: api/UsersRoles
        [HttpGet]
        public IQueryable<USR1> GetUSR1()
        {
            return UserRoleService.GetUSR1();
        }
        #endregion
        #region ENDPOINT GET: api/UsersRoles/{id}
        [HttpGet]
        [ResponseType(typeof(USR1))]
        public IHttpActionResult GetUSR1(int id)
        {
            var response = UserRoleService.GetUSR1ById(id);
            if (response == null)
            {
                return NotFound();
            }
            return Ok(response);
        }
        #endregion
        #endregion
        #region POST
        #region POST: api/UsersRoles
        // POST: api/UsersRoles
        [HttpPost]
        [ResponseType(typeof(USR1))]
        public IHttpActionResult PostUSR1(USR1 USR1)
        {
            var response = UserRoleService.PostUSR1(USR1);
            if (response.Equals(StatusServices.StatusService.Error.ToString()))
            {
                return BadRequest();
            }
            return Ok();
        }
        #endregion
        #endregion
        #region PUT
        #region PUT: api/UsersRoles/5
        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUSR1(USR1 USR1)
        {
            var Response = UserRoleService.PutUSR1(USR1);
            if (Response.Equals(StatusServices.StatusService.Succes.ToString()))
            {
                return Ok(StatusServices.StatusService.Succes.ToString());
            }
            return BadRequest(Response.ToString());
        }
        #endregion
        #endregion
        #region DELETE
        #region DELETE: api/UsersRoles/5
        [HttpDelete]
        [ResponseType(typeof(USR1))]
        public IHttpActionResult DeleteUSR1(int id)
        {
            var Response = UserRoleService.DeleteUSR1(id);
            if (Response.Equals(StatusServices.StatusService.Succes.ToString()))
            {
                return Ok(StatusServices.StatusService.Succes.ToString());
            }
            return BadRequest(StatusServices.StatusService.Error.ToString());
        }

        #endregion
        #endregion
        #region DIPOSE
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                UserRoleService.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}