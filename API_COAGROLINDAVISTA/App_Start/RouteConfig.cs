﻿using Swashbuckle.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace API_COAGROLINDAVISTA
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    //defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //    //defaults: new { controller = "swagger", action = "ui", id = UrlParameter.Optional }
            //);

            routes.MapHttpRoute(name: "swagger", routeTemplate: "", defaults: null, constraints: null, handler: new RedirectHandler((url => url.RequestUri.ToString()), "swagger"));
        }
    }
}
