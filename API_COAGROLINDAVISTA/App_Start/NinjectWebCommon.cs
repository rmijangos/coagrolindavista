[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(API_COAGROLINDAVISTA.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(API_COAGROLINDAVISTA.App_Start.NinjectWebCommon), "Stop")]

namespace API_COAGROLINDAVISTA.App_Start
{
    using System;
    using System.Data.Entity;
    using System.Web;
    using API_COAGROLINDAVISTA.Models;
    using API_COAGROLINDAVISTA.Repo;
    using API_COAGROLINDAVISTA.Repo.Interfaces;
    using API_COAGROLINDAVISTA.Services;
    using API_COAGROLINDAVISTA.Services.interfaces;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using Ninject.Web.Common.WebHost;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application.
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            #region Instances controller Sheds
            kernel.Bind<IshedRepo>().To<ShedRepo>();
            kernel.Bind<IShedService>().To<ShedService>();
            #endregion
            #region Instances controller Productions
            kernel.Bind<IProductionsRepo>().To<ProductionsRepo>();
            kernel.Bind<IProductionsService>().To<ProductionsService>();
            #endregion
            #region Instances controller GenresChickens
            kernel.Bind<IGenresChickensRepo>().To<GenresChickensRepo>();
            kernel.Bind<IGenresChickensService>().To<GenresChickensService>();
            #endregion
            #region Instances controller BussinesPartner
            kernel.Bind<ITypeBussinesPartnerRepo>().To<TypeBussinesPartnerRepo>();
            kernel.Bind<ITypeBussinesPartnerService>().To<TypeBussinesPartnerService>();
            #endregion
            #region Instances controller TypeGroupBussinesPartners
            kernel.Bind<ITypeGroupBussinesPartnersRepo>().To<TypeGroupBussinesPartnersRepo>();
            kernel.Bind<ITypeGroupBussinesPartnersService>().To<TypeGroupBussinesPartnersService>();
            #endregion
            #region Instances controller BussinesPartners
            kernel.Bind<IBussinesPartnersRepo>().To<BussinesPartnersRepo>();
            kernel.Bind<IBussinesPartnersService>().To<BussinesPartnersService>();
            #endregion
            #region Instances controller AddressesBussinesPartners
            kernel.Bind<IAddressBussinesPartnerRepo>().To<AddressBussinesPartnerRepo>();
            kernel.Bind<IAddressBussinesPartnerService>().To<AddressBussinesPartnerService>();
            #endregion
            #region Instances controller UsersRoles
            kernel.Bind<IUserRoleRepo>().To<UserRoleRepo>();
            kernel.Bind<IUserRoleService>().To<UserRoleService>();
            #endregion
            #region Instances controller Users
            kernel.Bind<IUserRepo>().To<UserRepo>();
            kernel.Bind<IUserService>().To<UserService>();
            #endregion
            #region Instances Generate JWT
            kernel.Bind<IGenerateJWTRepo>().To<GenerateJWTRepo>();
            kernel.Bind<IGenerateJWTService>().To<GenerateJWTService>();
            #endregion
            #region Instances Data base 
            kernel.Bind<COAGROLINDAVISTAEntities>().To<COAGROLINDAVISTAEntities>().InRequestScope();
            #endregion
        }
    }
}