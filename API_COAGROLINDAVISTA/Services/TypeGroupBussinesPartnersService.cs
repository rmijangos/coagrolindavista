﻿using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Repo;
using API_COAGROLINDAVISTA.Services.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_COAGROLINDAVISTA.Services
{
    public class TypeGroupBussinesPartnersService : ITypeGroupBussinesPartnersService
    {
        #region Variables in the scope Class TypeGroupBussinesPartnersRepo
        TypeGroupBussinesPartnersRepo TypeGroupBussinesPartnersRepo;
        #endregion
        #region Constructor DI Entities
        public TypeGroupBussinesPartnersService(TypeGroupBussinesPartnersRepo TypeGroupBussinesPartnersRepo)
        {
            this.TypeGroupBussinesPartnersRepo = TypeGroupBussinesPartnersRepo;
        }
        #endregion
        #region DELETE: api/TypeBussinesPartners/
        public string DeleteOTGB(int id)
        {
            String Response = String.Empty;
            if (TypeGroupBussinesPartnersRepo.Exists(id))
            {
                var Result = TypeGroupBussinesPartnersRepo.DeleteOTGB(id);
                if (Result)
                {
                    Response = StatusServices.StatusService.Succes.ToString();
                }
                else
                {
                    Response = StatusServices.StatusService.Error.ToString();
                }
            }
            return Response.ToString();
        }
        #endregion
        #region GET: api/TypeBussinesPartners
        public IQueryable<OTGB> GetOTGB()
        {
            return TypeGroupBussinesPartnersRepo.GetOTGB();
        }
        #endregion
        #region GET: api/TypeBussinesPartners/{id}
        public OTGB GetOTGBById(int id)
        {
            var response = TypeGroupBussinesPartnersRepo.GetOTGBById(id);
            return response;
        }
        #endregion
        #region POST: api/TypeBussinesPartners
        public string PostOTGB(OTGB TypeBussinesPartner)
        {
            var response = TypeGroupBussinesPartnersRepo.PostOTGB(TypeBussinesPartner);
            if (response.Equals(true))
            {
                return StatusServices.StatusService.Succes.ToString();
            }
            else
            {
                return StatusServices.StatusService.Error.ToString();
            }
        }
        #endregion
        #region PUT: api/TypeBussinesPartners/5
        public string PutOTGB(OTGB TypeBussinesPartner)
        {
            var Response = TypeGroupBussinesPartnersRepo.PutOTGB(TypeBussinesPartner);
            if (Response.Equals("succes"))
            {
                return StatusServices.StatusService.Succes.ToString();
            }
            return Response.ToString();
        }
        #endregion
        #region DIPOSE
        void ITypeGroupBussinesPartnersService.Dispose()
        {
            TypeGroupBussinesPartnersRepo.Dispose();
        }
        #endregion
    }
}