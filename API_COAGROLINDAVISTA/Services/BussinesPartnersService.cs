﻿using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Repo.Interfaces;
using API_COAGROLINDAVISTA.Services.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_COAGROLINDAVISTA.Services
{
    public class BussinesPartnersService : IBussinesPartnersService
    {
        #region Variables in the scope Class BussinesPartnersRepo
        IBussinesPartnersRepo BussinesPartnersRepo;
        #endregion
        #region Constructor DI Entities
        public BussinesPartnersService(IBussinesPartnersRepo BussinesPartnersRepo)
        {
            this.BussinesPartnersRepo = BussinesPartnersRepo;
        }
        #endregion
        #region DELETE: api/BussinesPartners/
        public string DeleteOBPR(int id)
        {
            String Response = String.Empty;
            if (BussinesPartnersRepo.Exists(id))
            {
                var Result = BussinesPartnersRepo.DeleteOBPR(id);
                if (Result)
                {
                    Response = StatusServices.StatusService.Succes.ToString();
                }
                else
                {
                    Response = StatusServices.StatusService.Error.ToString();
                }
            }
            return Response.ToString();
        }
        #endregion
        #region GET: api/BussinesPartners
        public IQueryable<OBPR> GetOBPR()
        {
            return BussinesPartnersRepo.GetOBPR();
        }
        #endregion
        #region GET: api/BussinesPartners/{id}
        public OBPR GetOBPRById(String id)
        {
            var response = BussinesPartnersRepo.GetOBPRById(id);
            return response;
        }
        #endregion
        #region POST: api/BussinesPartners
        public string PostOBPR(OBPR BussinesPartner)
        {
            var response = BussinesPartnersRepo.PostOBPR(BussinesPartner);
            if (response.Equals(true))
            {
                return StatusServices.StatusService.Succes.ToString();
            }
            else
            {
                return StatusServices.StatusService.Error.ToString();
            }
        }
        #endregion
        #region PUT: api/BussinesPartners/5
        public string PutOBPR(OBPR BussinesPartner)
        {
            var Response = BussinesPartnersRepo.PutOBPR(BussinesPartner);
            if (Response.Equals("succes"))
            {
                return StatusServices.StatusService.Succes.ToString();
            }
            return Response.ToString();
        }
        #endregion
        #region DIPOSE
        void IBussinesPartnersService.Dispose()
        {
            BussinesPartnersRepo.Dispose();
        }
        #endregion
    }
}