﻿using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Repo.Interfaces;
using API_COAGROLINDAVISTA.Services.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_COAGROLINDAVISTA.Services
{
    public class GenresChickensService : IGenresChickensService
    {
        #region Variables in the scope Class ShedRepo
        IGenresChickensRepo GnrChickenRepo;
        #endregion
        #region Constructor DI Entities
        public GenresChickensService(IGenresChickensRepo GnrChickenRepo)
        {
            this.GnrChickenRepo = GnrChickenRepo;
        }
        #endregion
        #region DELETE: api/GenresChickens/
        public string DeleteOCHK(int id)
        {
            String Response = String.Empty;
            if (GnrChickenRepo.Exists(id))
            {
                var Result = GnrChickenRepo.DeleteOCHK(id);
                if (Result)
                {
                    Response = StatusServices.StatusService.Succes.ToString();
                }
                else
                {
                    Response = StatusServices.StatusService.Error.ToString();
                }
            }
            return Response.ToString();
        }
        #endregion
        #region GET: api/GenresChickens
        public IQueryable<OCHK> GetOCHK()
        {
            return GnrChickenRepo.GetOCHK();
        }
        #endregion
        #region GET: api/GenresChickens/{id}
        public OCHK GetOCHKById(int id)
        {
            var response = GnrChickenRepo.GetOCHKById(id);
            return response;
        }
        #endregion
        #region POST: api/GenresChickens
        public string PostOCHK(OCHK GnrChicken)
        {
            var response = GnrChickenRepo.PostOCHK(GnrChicken);
            if (response.Equals(true))
            {
                return StatusServices.StatusService.Succes.ToString();
            }
            else
            {
                return StatusServices.StatusService.Error.ToString();
            }
        }
        #endregion
        #region PUT: api/GenresChickens/5
        public string PutOCHK(OCHK GnrChicken)
        {
            var Response = GnrChickenRepo.PutOCHK(GnrChicken);
            if (Response.Equals("succes"))
            {
                return StatusServices.StatusService.Succes.ToString();
            }
            return Response.ToString();
        }
        #endregion
        #region DIPOSE
        void IGenresChickensService.Dispose()
        {
            GnrChickenRepo.Dispose();
        }
        #endregion
    }
}