﻿using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Repo.Interfaces;
using API_COAGROLINDAVISTA.Services.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_COAGROLINDAVISTA.Services
{
    public class TypeBussinesPartnerService : ITypeBussinesPartnerService
    {
        #region Variables in the scope Class BussinesPartnerRepo
        ITypeBussinesPartnerRepo BussinesPartnerRepo;
        #endregion
        #region Constructor DI Entities
        public TypeBussinesPartnerService(ITypeBussinesPartnerRepo BussinesPartnerRepo)
        {
            this.BussinesPartnerRepo = BussinesPartnerRepo;
        }
        #endregion
        #region DELETE: api/BussinesPartners/
        public string DeleteOTBP(int id)
        {
            String Response = String.Empty;
            if (BussinesPartnerRepo.Exists(id))
            {
                var Result = BussinesPartnerRepo.DeleteOTBP(id);
                if (Result)
                {
                    Response = StatusServices.StatusService.Succes.ToString();
                }
                else
                {
                    Response = StatusServices.StatusService.Error.ToString();
                }
            }
            return Response.ToString();
        }
        #endregion
        #region GET: api/BussinesPartners
        public IQueryable<OTBP> GetOTBP()
        {
            return BussinesPartnerRepo.GetOTBP();
        }
        #endregion
        #region GET: api/BussinesPartners/{id}
        public OTBP GetOTBPById(int id)
        {
            var response = BussinesPartnerRepo.GetOTBPById(id);
            return response;
        }
        #endregion
        #region POST: api/BussinesPartners
        public string PostOTBP(OTBP BussinesPartners)
        {
            var response = BussinesPartnerRepo.PostOTBP(BussinesPartners);
            if (response.Equals(true))
            {
                return StatusServices.StatusService.Succes.ToString();
            }
            else
            {
                return StatusServices.StatusService.Error.ToString();
            }
        }
        #endregion
        #region PUT: api/BussinesPartners/5
        public string PutOTBP(OTBP BussinesPartners)
        {
            var Response = BussinesPartnerRepo.PutOTBP(BussinesPartners);
            if (Response.Equals("succes"))
            {
                return StatusServices.StatusService.Succes.ToString();
            }
            return Response.ToString();
        }
        #endregion
        #region DIPOSE
        void ITypeBussinesPartnerService.Dispose()
        {
            BussinesPartnerRepo.Dispose();
        }
        #endregion
    }
}