﻿using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Repo.Interfaces;
using API_COAGROLINDAVISTA.Services.interfaces;
using System;
using System.Linq;

namespace API_COAGROLINDAVISTA.Services
{
    public class ShedService : IShedService
    {
        #region Variables in the scope Class ShedRepo
        IshedRepo ShedRepo;
        #endregion
        #region Constructor DI Entities
        public ShedService(IshedRepo ShedRepo)
        {
            this.ShedRepo = ShedRepo;
        }
        #endregion
        #region DELETE: api/Shelds/
        public string DeleteOSHE(int id)
        {
            String Response = String.Empty;
            if (ShedRepo.Exists(id))
            {
                var Result = ShedRepo.DeleteOSHE(id);
                if (Result)
                {
                    Response = StatusServices.StatusService.Succes.ToString();
                }
                else
                {
                    Response = StatusServices.StatusService.Error.ToString();
                }
            }
            return Response.ToString();
        }
        #endregion
        #region GET: api/Shelds
        public IQueryable<OSHE> GetOSHE()
        {
            return ShedRepo.GetOSHE();
        }
        #endregion
        #region GET: api/Shelds/{id}
        public OSHE GetOSHEById(int id)
        {
            var response = ShedRepo.GetOSHEById(id);
            return response;
        }
        #endregion
        #region POST: api/Shelds
        public string PostOSHE(OSHE Shed)
        {
            var response = ShedRepo.PostOSHE(Shed);
            if (response.Equals(true))
            {
                return StatusServices.StatusService.Succes.ToString();
            }
            else
            {
                return StatusServices.StatusService.Error.ToString();
            }
        }
        #endregion
        #region PUT: api/Shelds/5
        public string PutOSHE(OSHE Shed)
        {
            var Response = ShedRepo.PutOSHE(Shed);
            if (Response.Equals("succes"))
            {
                return StatusServices.StatusService.Succes.ToString();
            }
            return Response.ToString();
        }
        #endregion
        #region DIPOSE
        void IShedService.Dispose()
        {
            ShedRepo.Dispose();
        }
        #endregion
    }
}