﻿using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Repo.Interfaces;
using API_COAGROLINDAVISTA.Services.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_COAGROLINDAVISTA.Services
{
    public class UserService : IUserService
    {
        #region Variables in the scope Class UserRepo
        IUserRepo UserRepo;
        #endregion
        #region Constructor DI Entities
        public UserService(IUserRepo UserRepo)
        {
            this.UserRepo = UserRepo;
        }
        #endregion
        #region DELETE: api/Users/
        public string DeleteOUSR(int id)
        {
            String Response = String.Empty;
            if (UserRepo.Exists(id))
            {
                var Result = UserRepo.DeleteOUSR(id);
                if (Result)
                {
                    Response = StatusServices.StatusService.Succes.ToString();
                }
                else
                {
                    Response = StatusServices.StatusService.Error.ToString();
                }
            }
            return Response.ToString();
        }
        #endregion
        #region GET: api/Users
        public IQueryable<OUSR> GetOUSR()
        {
            return UserRepo.GetOUSR();
        }
        #endregion
        #region GET: api/Users/{id}
        public OUSR GetOUSRById(int id)
        {
            var response = UserRepo.GetOUSRById(id);
            return response;
        }
        #endregion
        #region GET: api/Users/{user}/{pass}
        public OUSR GetOUSRByUserAndPass(String User, String Pass)
        {
            var response = UserRepo.GetOUSRByUserAndPass(User, Pass);
            return response;
        }
        #endregion
        #region POST: api/Users
        public string PostOUSR(OUSR Users)
        {
            var response = UserRepo.PostOUSR(Users);
            if (response.Equals(true))
            {
                return StatusServices.StatusService.Succes.ToString();
            }
            else
            {
                return StatusServices.StatusService.Error.ToString();
            }
        }
        #endregion
        #region PUT: api/Users/5
        public string PutOUSR(OUSR Users)
        {
            var Response = UserRepo.PutOUSR(Users);
            if (Response.Equals("succes"))
            {
                return StatusServices.StatusService.Succes.ToString();
            }
            return Response.ToString();
        }
        #endregion
        #region DIPOSE
        void IUserService.Dispose()
        {
            UserRepo.Dispose();
        }
        #endregion
    }
}