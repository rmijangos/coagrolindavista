﻿using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Repo.Interfaces;
using API_COAGROLINDAVISTA.Services.interfaces;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Web;

namespace API_COAGROLINDAVISTA.Services
{
    public class GenerateJWTService : IGenerateJWTService
    {
        #region Variables in the scope Class UserRepo
        IGenerateJWTRepo GenJWTRepo;
        #endregion
        #region Constructor DI Entities
        public GenerateJWTService(IGenerateJWTRepo GenJWTRepo)
        {
            this.GenJWTRepo = GenJWTRepo;
        }
        #endregion
        #region METHOD AuthUser
        public UserInfo AuthUser(string usuario, string password)
        {
            var User = GenJWTRepo.GetOUSRByUserAndPass(usuario, password);

            if (User != null)
            {
                return new UserInfo()
                {
                    Id = User.IdUser,
                    Name = User.UserName,
                    LastName = User.UserLastName,
                    Role = User.IdRole.ToString()
                };
            }
            return  null;
        }
        #endregion
        #region METHOD GenerateToken 
        public string GenerateToken(UserInfo UserInfo)
        {
            var _KeySecret = ConfigurationManager.AppSettings["ClaveSecreta"];
            var _Issuer = ConfigurationManager.AppSettings["Issuer"];
            var _Audience = ConfigurationManager.AppSettings["Audience"];
            if (!Int32.TryParse(ConfigurationManager.AppSettings["Expires"], out int _Expires))
                _Expires = 24;

            //Header
            var _symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_KeySecret));
            var _signingCredentials = new SigningCredentials(_symmetricSecurityKey, SecurityAlgorithms.HmacSha256);
            var _Header = new JwtHeader(_signingCredentials);

            //Claims
            var _Claims = new[] {
                new Claim(JwtRegisteredClaimNames.Jti, UserInfo.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.NameId, UserInfo.Id.ToString()),
                new Claim("Name", UserInfo.Name),
                new Claim("LastName", UserInfo.LastName),
                new Claim(ClaimTypes.Role, UserInfo.Role)
            };

            //Payload
            var _Payload = new JwtPayload(
                    issuer: _Issuer,
                    audience: _Audience,
                    claims: _Claims,
                    notBefore: DateTime.UtcNow,
                    expires: DateTime.UtcNow.AddHours(_Expires)
                );

            //Generating Token
            var _Token = new JwtSecurityToken(
                    _Header,
                    _Payload
                );
            return new JwtSecurityTokenHandler().WriteToken(_Token);
        }
        #endregion
    }
}
