﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_COAGROLINDAVISTA.Services.interfaces
{
    public interface IAddressBussinesPartnerService
    {
        IQueryable<OBPA> GetOBPA();
        OBPA GetOBPAById(int id);
        String PutOBPA(OBPA AddressesBussinesPartners);
        String PostOBPA(OBPA AddressesBussinesPartners);
        String DeleteOBPA(int id);
        void Dispose();
    }
}
