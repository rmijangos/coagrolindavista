﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_COAGROLINDAVISTA.Services.interfaces
{
    public interface IBussinesPartnersService
    {
        IQueryable<OBPR> GetOBPR();
        OBPR GetOBPRById(String id);
        String PutOBPR(OBPR BussinesPartner);
        String PostOBPR(OBPR BussinesPartner);
        String DeleteOBPR(int id);
        void Dispose();
    }
}
