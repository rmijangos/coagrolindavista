﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_COAGROLINDAVISTA.Services.interfaces
{
    public interface IUserRoleService
    {
        IQueryable<USR1> GetUSR1();
        USR1 GetUSR1ById(int id);
        String PutUSR1(USR1 UserRole);
        String PostUSR1(USR1 UserRole);
        String DeleteUSR1(int id);
        void Dispose();
    }
}
