﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Linq;

namespace API_COAGROLINDAVISTA.Services.interfaces
{
    public interface IGenresChickensService
    {
        IQueryable<OCHK> GetOCHK();
        OCHK GetOCHKById(int id);
        String PutOCHK(OCHK GnrChicken);
        String PostOCHK(OCHK GnrChicken);
        String DeleteOCHK(int id);
        void Dispose();
    }
}
