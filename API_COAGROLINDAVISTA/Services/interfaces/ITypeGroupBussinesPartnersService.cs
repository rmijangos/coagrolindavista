﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_COAGROLINDAVISTA.Services.interfaces
{
    public interface ITypeGroupBussinesPartnersService
    {
        IQueryable<OTGB> GetOTGB();
        OTGB GetOTGBById(int id);
        String PutOTGB(OTGB TypeGroupBussinesPartner);
        String PostOTGB(OTGB TypeGroupBussinesPartner);
        String DeleteOTGB(int id);
        void Dispose();
    }
}
