﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_COAGROLINDAVISTA.Services.interfaces
{
    public interface IUserService
    {
        IQueryable<OUSR> GetOUSR();
        OUSR GetOUSRById(int id);
        OUSR GetOUSRByUserAndPass(String User, String Pass);
        String PutOUSR(OUSR User);
        String PostOUSR(OUSR User);
        String DeleteOUSR(int id);
        void Dispose();
    }
}
