﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_COAGROLINDAVISTA.Services.interfaces
{
    public interface IProductionsService
    {
        IQueryable<OPRO> GetOPRO();
        OPRO GetOPROById(int id);
        String PutOPRO(OPRO Production);
        String PostOPRO(OPRO Production);
        String DeleteOPRO(int id);
        void Dispose();
    }
}
