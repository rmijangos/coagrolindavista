﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_COAGROLINDAVISTA.Services.interfaces
{
    public interface ITypeBussinesPartnerService
    {
        IQueryable<OTBP> GetOTBP();
        OTBP GetOTBPById(int id);
        String PutOTBP(OTBP TypeBussinesPartner);
        String PostOTBP(OTBP TypeBussinesPartner);
        String DeleteOTBP(int id);
        void Dispose();
    }
}
