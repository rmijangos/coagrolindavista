﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_COAGROLINDAVISTA.Services.interfaces
{
    public interface IGenerateJWTService
    {
        UserInfo AuthUser(string usuario, string password);
        String GenerateToken(UserInfo UserInfo);

    }
}
