﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Linq;

namespace API_COAGROLINDAVISTA.Services.interfaces
{
    public interface IShedService
    {
        IQueryable<OSHE> GetOSHE();
        OSHE GetOSHEById(int id);
        String PutOSHE(OSHE Shed);
        String PostOSHE(OSHE Shed);
        String DeleteOSHE(int id);
        void Dispose();
    }
}
