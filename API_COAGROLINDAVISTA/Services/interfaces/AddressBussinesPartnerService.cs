﻿using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Repo.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_COAGROLINDAVISTA.Services.interfaces
{
    public class AddressBussinesPartnerService : IAddressBussinesPartnerService
    {
        #region Variables in the scope Class AddressBussinesPartnerRepo
        IAddressBussinesPartnerRepo AddressBussinesPartnerRepo;
        #endregion
        #region Constructor DI Entities
        public AddressBussinesPartnerService(IAddressBussinesPartnerRepo AddressBussinesPartnerRepo)
        {
            this.AddressBussinesPartnerRepo = AddressBussinesPartnerRepo;
        }
        #endregion
        #region DELETE: api/AddressesBussinesPartners/
        public string DeleteOBPA(int id)
        {
            String Response = String.Empty;
            if (AddressBussinesPartnerRepo.Exists(id))
            {
                var Result = AddressBussinesPartnerRepo.DeleteOBPA(id);
                if (Result)
                {
                    Response = StatusServices.StatusService.Succes.ToString();
                }
                else
                {
                    Response = StatusServices.StatusService.Error.ToString();
                }
            }
            return Response.ToString();
        }
        #endregion
        #region GET: api/AddressesBussinesPartners
        public IQueryable<OBPA> GetOBPA()
        {
            return AddressBussinesPartnerRepo.GetOBPA();
        }
        #endregion
        #region GET: api/AddressesBussinesPartners/{id}
        public OBPA GetOBPAById(int id)
        {
            var response = AddressBussinesPartnerRepo.GetOBPAById(id);
            return response;
        }
        #endregion
        #region POST: api/AddressesBussinesPartners
        public string PostOBPA(OBPA AddressBussinesPartner)
        {
            var response = AddressBussinesPartnerRepo.PostOBPA(AddressBussinesPartner);
            if (response.Equals(true))
            {
                return StatusServices.StatusService.Succes.ToString();
            }
            else
            {
                return StatusServices.StatusService.Error.ToString();
            }
        }
        #endregion
        #region PUT: api/AddressesBussinesPartners/5
        public string PutOBPA(OBPA AddressBussinesPartner)
        {
            var Response = AddressBussinesPartnerRepo.PutOBPA(AddressBussinesPartner);
            if (Response.Equals("succes"))
            {
                return StatusServices.StatusService.Succes.ToString();
            }
            return Response.ToString();
        }
        #endregion
        #region DIPOSE
        void IAddressBussinesPartnerService.Dispose()
        {
            AddressBussinesPartnerRepo.Dispose();
        }
        #endregion
    }
}