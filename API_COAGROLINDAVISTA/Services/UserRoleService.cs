﻿using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Repo.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_COAGROLINDAVISTA.Services.interfaces
{
    public class UserRoleService : IUserRoleService
    {
        #region Variables in the scope Class UserRoleRepo
        IUserRoleRepo UserRoleRepo;
        #endregion
        #region Constructor DI Entities
        public UserRoleService(IUserRoleRepo UserRoleRepo)
        {
            this.UserRoleRepo = UserRoleRepo;
        }
        #endregion
        #region DELETE: api/UsersRoles/
        public string DeleteUSR1(int id)
        {
            String Response = String.Empty;
            if (UserRoleRepo.Exists(id))
            {
                var Result = UserRoleRepo.DeleteUSR1(id);
                if (Result)
                {
                    Response = StatusServices.StatusService.Succes.ToString();
                }
                else
                {
                    Response = StatusServices.StatusService.Error.ToString();
                }
            }
            return Response.ToString();
        }
        #endregion
        #region GET: api/UsersRoles
        public IQueryable<USR1> GetUSR1()
        {
            return UserRoleRepo.GetUSR1();
        }
        #endregion
        #region GET: api/UsersRoles/{id}
        public USR1 GetUSR1ById(int id)
        {
            var response = UserRoleRepo.GetUSR1ById(id);
            return response;
        }
        #endregion
        #region POST: api/UsersRoles
        public string PostUSR1(USR1 UserRole)
        {
            var response = UserRoleRepo.PostUSR1(UserRole);
            if (response.Equals(true))
            {
                return StatusServices.StatusService.Succes.ToString();
            }
            else
            {
                return StatusServices.StatusService.Error.ToString();
            }
        }
        #endregion
        #region PUT: api/UsersRoles/5
        public string PutUSR1(USR1 UserRole)
        {
            var Response = UserRoleRepo.PutUSR1(UserRole);
            if (Response.Equals("succes"))
            {
                return StatusServices.StatusService.Succes.ToString();
            }
            return Response.ToString();
        }
        #endregion
        #region DIPOSE
        void IUserRoleService.Dispose()
        {
            UserRoleRepo.Dispose();
        }
        #endregion
    }
}