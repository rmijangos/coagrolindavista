﻿using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Repo.Interfaces;
using API_COAGROLINDAVISTA.Services.interfaces;
using System;
using System.Linq;

namespace API_COAGROLINDAVISTA.Services
{
    public class ProductionsService : IProductionsService
    {
        #region Variables in the scope Class ShedRepo
        IProductionsRepo ProductionRepo;
        #endregion
        #region Constructor DI Entities
        public ProductionsService(IProductionsRepo ProductionRepo)
        {
            this.ProductionRepo = ProductionRepo;
        }
        #endregion
        #region DELETE: api/Productions/
        public string DeleteOPRO(int id)
        {
            String Response = String.Empty;
            if (ProductionRepo.Exists(id))
            {
                var Result = ProductionRepo.DeleteOPRO(id);
                if (Result)
                {
                    Response = StatusServices.StatusService.Succes.ToString();
                }
                else
                {
                    Response = StatusServices.StatusService.Error.ToString();
                }
            }
            return Response.ToString();
        }
        #endregion
        #region GET: api/Productions
        public IQueryable<OPRO> GetOPRO()
        {
            return ProductionRepo.GetOPRO();
        }
        #endregion
        #region GET: api/Productions/{id}
        public OPRO GetOPROById(int id)
        {
            var response = ProductionRepo.GetOPROById(id);
            return response;
        }
        #endregion
        #region POST: api/Productions
        public string PostOPRO(OPRO Shed)
        {
            var response = ProductionRepo.PostOPRO(Shed);
            if (response.Equals(true))
            {
                return StatusServices.StatusService.Succes.ToString();
            }
            else
            {
                return StatusServices.StatusService.Error.ToString();
            }
        }
        #endregion
        #region PUT: api/Productions/5
        public string PutOPRO(OPRO Production)
        {
            var Response = ProductionRepo.PutOPRO(Production);
            if (Response.Equals("succes"))
            {
                return StatusServices.StatusService.Succes.ToString();
            }
            return Response.ToString();
        }
        #endregion
        #region DIPOSE
        void IProductionsService.Dispose()
        {
            ProductionRepo.Dispose();
        }
        #endregion
    }
}