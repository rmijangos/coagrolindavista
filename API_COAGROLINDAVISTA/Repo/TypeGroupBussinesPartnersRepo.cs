﻿using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Repo.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace API_COAGROLINDAVISTA.Repo
{
    public class TypeGroupBussinesPartnersRepo : ITypeGroupBussinesPartnersRepo
    {
        #region Variables in the scope Class TypeGroupBussinesPartnersRepo
        private COAGROLINDAVISTAEntities db;
        #endregion
        #region Constructor DI Entities
        public TypeGroupBussinesPartnersRepo(COAGROLINDAVISTAEntities db)
        {
            this.db = db;
        }
        #endregion
        #region DELETE: api/TypeGroupBussinesPartners/
        public bool DeleteOTGB(int id)
        {
            OTGB TypeGroupBussinesPartner = db.OTGB.Find(id);
            if (TypeGroupBussinesPartner == null)
            {
                return false;
            }
            db.OTGB.Remove(TypeGroupBussinesPartner);
            db.SaveChanges();
            return true;
        }
        #endregion
        #region GET: api/TypeGroupBussinesPartners
        public IQueryable<OTGB> GetOTGB()
        {
            return db.OTGB;
        }
        #endregion
        #region GET: api/TypeGroupBussinesPartners/{id}
        public OTGB GetOTGBById(int id)
        {
            OTGB TypeGroupBussinesPartner = db.OTGB.Find(id);
            return TypeGroupBussinesPartner;
        }
        #endregion
        #region POST: api/TypeGroupBussinesPartners
        public bool PostOTGB(OTGB TypeGroupBussinesPartner)
        {
            try
            {
                db.OTGB.Add(TypeGroupBussinesPartner);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
        #region PUT: api/TypeGroupBussinesPartners/5
        public String PutOTGB(OTGB TypeGroupBussinesPartner)
        {
            try
            {
                db.Entry(TypeGroupBussinesPartner).State = EntityState.Modified;
                db.SaveChanges();
                return "succes";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        #endregion
        #region EXISTS CHECKING ID
        public bool Exists(int id)
        {
            return db.OTGB.Count(e => e.IdGBP.Equals(id)) > 0;
        }
        #endregion
        #region DISPOSE
        public void Dispose()
        {
            db.Dispose();
        }
        #endregion
    }
}