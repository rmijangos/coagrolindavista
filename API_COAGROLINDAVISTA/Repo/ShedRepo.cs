﻿using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Repo.Interfaces;
using System;
using System.Data.Entity;
using System.Linq;

namespace API_COAGROLINDAVISTA.Repo
{
    public class ShedRepo : IshedRepo
    {
        #region Variables in the scope Class ShedRepo
        private COAGROLINDAVISTAEntities db;
        #endregion
        #region Constructor DI Entities
        public ShedRepo(COAGROLINDAVISTAEntities db) {
            this.db = db;
        }
        #endregion
        #region DELETE: api/Shelds/
        public bool DeleteOSHE(int id)
        {
            OSHE Shed = db.OSHE.Find(id);
            if (Shed == null)
            {
                return false;
            }
            db.OSHE.Remove(Shed);
            db.SaveChanges();
            return true;
        }
        #endregion
        #region GET: api/Shelds
        public IQueryable<OSHE> GetOSHE()
        {
            return db.OSHE;
        }
        #endregion
        #region GET: api/Shelds/{id}
        public OSHE GetOSHEById(int id)
        {
            OSHE Shed = db.OSHE.Find(id);
            return Shed;
        }
        #endregion
        #region POST: api/Shelds
        public bool PostOSHE(OSHE Shed)
        {
            try
            {
                db.OSHE.Add(Shed);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
        #region PUT: api/Shelds/5
        public String PutOSHE(OSHE Shed)
        {
            try
            {
                db.Entry(Shed).State = EntityState.Modified;
                db.SaveChanges();
                return "succes";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        #endregion
        #region EXISTS CHECKING ID
        public bool Exists(int id)
        {
            return db.OSHE.Count(e => e.IdShed.Equals(id)) > 0;
        }
        #endregion
        #region DISPOSE
        public void Dispose()
        {
            db.Dispose();
        }
        #endregion
    }
}