﻿using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Repo.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace API_COAGROLINDAVISTA.Repo
{
    public class UserRepo : IUserRepo
    {
        #region Variables in the scope Class UserRepo
        private COAGROLINDAVISTAEntities db;
        #endregion
        #region Constructor DI Entities
        public UserRepo(COAGROLINDAVISTAEntities db)
        {
            this.db = db;
        }
        #endregion
        #region DELETE: api/Users/
        public bool DeleteOUSR(int id)
        {
            OUSR User = db.OUSR.Find(id);
            if (User == null)
            {
                return false;
            }
            db.OUSR.Remove(User);
            db.SaveChanges();
            return true;
        }
        #endregion
        #region GET: api/Users
        public IQueryable<OUSR> GetOUSR()
        {
            return db.OUSR;
        }
        #endregion
        #region GET: api/Users/{id}
        public OUSR GetOUSRById(int id)
        {
            OUSR User = db.OUSR.Find(id);
            return User;
        }
        #endregion
        #region GET: api/Users/{user},{pass}
        public OUSR GetOUSRByUserAndPass(String User, String Pass)
        {
            using (db)
            {
                OUSR response = null;
                try
                {
                    var query = db.OUSR.Where(s => s.NickName == User).Where(s => s.UserPassword == Pass).Select(c => c.IdUser).FirstOrDefault();
                    response = GetOUSRById(query);
                    return response;
                }
                catch (Exception)
                {
                    return response;
                }
                return response;
            }   
        }
        #endregion
        #region POST: api/Users
        public bool PostOUSR(OUSR User)
        {
            try
            {
                db.OUSR.Add(User);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
        #region PUT: api/Users/5
        public String PutOUSR(OUSR User)
        {
            try
            {
                db.Entry(User).State = EntityState.Modified;
                db.SaveChanges();
                return "succes";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        #endregion
        #region EXISTS CHECKING ID
        public bool Exists(int id)
        {
            return db.OUSR.Count(e => e.IdUser.Equals(id)) > 0;
        }
        #endregion
        #region DISPOSE
        public void Dispose()
        {
            db.Dispose();
        }
        #endregion
    }
}