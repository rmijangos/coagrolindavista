﻿using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Repo.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace API_COAGROLINDAVISTA.Repo
{
    public class UserRoleRepo : IUserRoleRepo
    {
        #region Variables in the scope Class UserRoleRepo
        private COAGROLINDAVISTAEntities db;
        #endregion
        #region Constructor DI Entities
        public UserRoleRepo(COAGROLINDAVISTAEntities db)
        {
            this.db = db;
        }
        #endregion
        #region DELETE: api/UsersRoles/
        public bool DeleteUSR1(int id)
        {
            USR1 UserRole = db.USR1.Find(id);
            if (UserRole == null)
            {
                return false;
            }
            db.USR1.Remove(UserRole);
            db.SaveChanges();
            return true;
        }
        #endregion
        #region GET: api/UsersRoles
        public IQueryable<USR1> GetUSR1()
        {
            return db.USR1;
        }
        #endregion
        #region GET: api/UsersRoles/{id}
        public USR1 GetUSR1ById(int id)
        {
            USR1 UserRole = db.USR1.Find(id);
            return UserRole;
        }
        #endregion
        #region POST: api/UsersRoles
        public bool PostUSR1(USR1 UserRole)
        {
            try
            {
                db.USR1.Add(UserRole);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
        #region PUT: api/UsersRoles/5
        public String PutUSR1(USR1 UserRole)
        {
            try
            {
                db.Entry(UserRole).State = EntityState.Modified;
                db.SaveChanges();
                return "succes";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        #endregion
        #region EXISTS CHECKING ID
        public bool Exists(int id)
        {
            return db.USR1.Count(e => e.IdRole.Equals(id)) > 0;
        }
        #endregion
        #region DISPOSE
        public void Dispose()
        {
            db.Dispose();
        }
        #endregion
    }
}