﻿using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Repo.Interfaces;
using System;
using System.Data.Entity;
using System.Linq;

namespace API_COAGROLINDAVISTA.Repo
{
    public class GenresChickensRepo : IGenresChickensRepo
    {
        #region Variables in the scope Class ShedRepo
        private COAGROLINDAVISTAEntities db;
        #endregion
        #region Constructor DI Entities
        public GenresChickensRepo(COAGROLINDAVISTAEntities db)
        {
            this.db = db;
        }
        #endregion
        #region DELETE: api/GenresChickens/
        public bool DeleteOCHK(int id)
        {
            OCHK GnrChicken = db.OCHK.Find(id);
            if (GnrChicken == null)
            {
                return false;
            }
            db.OCHK.Remove(GnrChicken);
            db.SaveChanges();
            return true;
        }
        #endregion
        #region GET: api/GenresChickens
        public IQueryable<OCHK> GetOCHK()
        {
            return db.OCHK;
        }
        #endregion
        #region GET: api/GenresChickens/{id}
        public OCHK GetOCHKById(int id)
        {
            OCHK GnrChicken = db.OCHK.Find(id);
            return GnrChicken;
        }
        #endregion
        #region POST: api/GenresChickens
        public bool PostOCHK(OCHK GnrChicken)
        {
            try
            {
                db.OCHK.Add(GnrChicken);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
        #region PUT: api/GenresChickens/{id}
        public String PutOCHK(OCHK GnrChicken)
        {
            try
            {
                db.Entry(GnrChicken).State = EntityState.Modified;
                db.SaveChanges();
                return "succes";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        #endregion
        #region EXISTS CHECKING ID
        public bool Exists(int id)
        {
            return db.OCHK.Count(e => e.IdChicken.Equals(id)) > 0;
        }
        #endregion
        #region DISPOSE
        public void Dispose()
        {
            db.Dispose();
        }
        #endregion
    }
}