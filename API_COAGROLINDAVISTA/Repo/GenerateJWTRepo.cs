﻿using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Repo.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_COAGROLINDAVISTA.Repo
{
    public class GenerateJWTRepo : IGenerateJWTRepo
    {
        #region Variables in the scope Class UserRepo
        private COAGROLINDAVISTAEntities db;
        #endregion
        #region Constructor DI Entities
        public GenerateJWTRepo(COAGROLINDAVISTAEntities db)
        {
            this.db = db;
        }
        #endregion

        public OUSR GetOUSRById(int id)
        {
            OUSR User = db.OUSR.Find(id);
            return User;
        }
        public OUSR GetOUSRByUserAndPass(String User, String Pass)
        {
            using (db)
            {
                OUSR response = null;
                try
                {
                    var query = db.OUSR.Where(s => s.NickName == User).Where(s => s.UserPassword == Pass).Select(c => c.IdUser).FirstOrDefault();
                    response = GetOUSRById(query);
                    return response;
                }
                catch (Exception)
                {
                    return response;
                }
                return response;
            }
        }
    }
}