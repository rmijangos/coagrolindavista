﻿using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Repo.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace API_COAGROLINDAVISTA.Repo
{
    public class BussinesPartnersRepo : IBussinesPartnersRepo
    {
        #region Variables in the scope Class ShedRepo
        private COAGROLINDAVISTAEntities db;
        #endregion
        #region Constructor DI Entities
        public BussinesPartnersRepo(COAGROLINDAVISTAEntities db)
        {
            this.db = db;
        }
        #endregion
        #region DELETE: api/BussinesPartners/
        public bool DeleteOBPR(int id)
        {
            OBPR BussinesPartners = db.OBPR.Find(id);
            if (BussinesPartners == null)
            {
                return false;
            }
            db.OBPR.Remove(BussinesPartners);
            db.SaveChanges();
            return true;
        }
        #endregion
        #region GET: api/BussinesPartners
        public IQueryable<OBPR> GetOBPR()
        {
            return db.OBPR;
        }
        #endregion
        #region GET: api/BussinesPartners/{id}
        public OBPR GetOBPRById(String id)
        {
            OBPR BussinesPartners = db.OBPR.Find(id);
            return BussinesPartners;
        }
        #endregion
        #region POST: api/BussinesPartners
        public bool PostOBPR(OBPR BussinesPartners)
        {
            try
            {
                db.OBPR.Add(BussinesPartners);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
        #region PUT: api/BussinesPartners/5
        public String PutOBPR(OBPR BussinesPartners)
        {
            try
            {
                db.Entry(BussinesPartners).State = EntityState.Modified;
                db.SaveChanges();
                return "succes";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        #endregion
        #region EXISTS CHECKING ID
        public bool Exists(int id)
        {
            return db.OBPR.Count(e => e.IdCardCode.Equals(id)) > 0;
        }
        #endregion
        #region DISPOSE
        public void Dispose()
        {
            db.Dispose();
        }
        #endregion
    }
}