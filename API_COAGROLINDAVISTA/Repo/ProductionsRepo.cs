﻿using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Repo.Interfaces;
using System;
using System.Data.Entity;
using System.Linq;

namespace API_COAGROLINDAVISTA.Repo
{
    public class ProductionsRepo : IProductionsRepo
    {
        #region Variables in the scope Class ShedRepo
        private COAGROLINDAVISTAEntities db;
        #endregion
        #region Constructor DI Entities
        public ProductionsRepo(COAGROLINDAVISTAEntities db)
        {
            this.db = db;
        }
        #endregion
        #region DELETE: api/Productions/
        public bool DeleteOPRO(int id)
        {
            OPRO Production = db.OPRO.Find(id);
            if (Production == null)
            {
                return false;
            }
            db.OPRO.Remove(Production);
            db.SaveChanges();
            return true;
        }
        #endregion
        #region GET: api/Productions
        public IQueryable<OPRO> GetOPRO()
        {
            return db.OPRO;
        }
        #endregion
        #region GET: api/Productions/{id}
        public OPRO GetOPROById(int id)
        {
            OPRO Production = db.OPRO.Find(id);
            return Production;
        }
        #endregion
        #region POST: api/Productions
        public bool PostOPRO(OPRO Production)
        {
            try
            {
                db.OPRO.Add(Production);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
        #region PUT: api/Productions/5
        public String PutOPRO(OPRO Production)
        {
            try
            {
                db.Entry(Production).State = EntityState.Modified;
                db.SaveChanges();
                return "succes";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        #endregion
        #region EXISTS CHECKING ID
        public bool Exists(int id)
        {
            return db.OPRO.Count(e => e.IdProd.Equals(id)) > 0;
        }
        #endregion
        #region DISPOSE
        public void Dispose()
        {
            db.Dispose();
        }
        #endregion
    }
}