﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_COAGROLINDAVISTA.Repo.Interfaces
{
    public interface IUserRoleRepo
    {
        IQueryable<USR1> GetUSR1();
        USR1 GetUSR1ById(int id);
        String PutUSR1(USR1 UserRole);
        bool PostUSR1(USR1 UserRole);
        bool DeleteUSR1(int id);
        bool Exists(int id);
        void Dispose();
    }
}
