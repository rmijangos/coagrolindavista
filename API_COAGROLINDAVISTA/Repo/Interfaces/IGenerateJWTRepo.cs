﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_COAGROLINDAVISTA.Repo.Interfaces
{
    public interface IGenerateJWTRepo
    {
        OUSR GetOUSRById(int id);
        OUSR GetOUSRByUserAndPass(String User, String Pass);
    }
}
