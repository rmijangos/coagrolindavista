﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_COAGROLINDAVISTA.Repo.Interfaces
{
    public interface IAddressBussinesPartnerRepo
    {
        IQueryable<OBPA> GetOBPA();
        OBPA GetOBPAById(int id);
        String PutOBPA(OBPA AddressBussinesPartner);
        bool PostOBPA(OBPA AddressBussinesPartner);
        bool DeleteOBPA(int id);
        bool Exists(int id);
        void Dispose();
    }
}
