﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_COAGROLINDAVISTA.Repo.Interfaces
{
    public interface ITypeGroupBussinesPartnersRepo
    {
        IQueryable<OTGB> GetOTGB();
        OTGB GetOTGBById(int id);
        String PutOTGB(OTGB TypeGroupBussinesPartner);
        bool PostOTGB(OTGB TypeGroupBussinesPartner);
        bool DeleteOTGB(int id);
        bool Exists(int id);
        void Dispose();
    }
}
