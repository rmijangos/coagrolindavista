﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_COAGROLINDAVISTA.Repo.Interfaces
{
    public interface IBussinesPartnersRepo
    {
        IQueryable<OBPR> GetOBPR();
        OBPR GetOBPRById(String id);
        String PutOBPR(OBPR BussinesPartners);
        bool PostOBPR(OBPR BussinesPartners);
        bool DeleteOBPR(int id);
        bool Exists(int id);
        void Dispose();
    }
}
