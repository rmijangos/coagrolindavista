﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Data.Entity;
using System.Linq;

namespace API_COAGROLINDAVISTA.Repo.Interfaces
{
    public class AddressBussinesPartnerRepo : IAddressBussinesPartnerRepo
    {
        #region Variables in the scope Class AddressBussinesPartnerRepo
        private COAGROLINDAVISTAEntities db;
        #endregion
        #region Constructor DI Entities
        public AddressBussinesPartnerRepo(COAGROLINDAVISTAEntities db)
        {
            this.db = db;
        }
        #endregion
        #region DELETE: api/AddressesBussinesPartners/
        public bool DeleteOBPA(int id)
        {
            OBPA AddressBussinesPartner = db.OBPA.Find(id);
            if (AddressBussinesPartner == null)
            {
                return false;
            }
            db.OBPA.Remove(AddressBussinesPartner);
            db.SaveChanges();
            return true;
        }
        #endregion
        #region GET: api/AddressesBussinesPartners
        public IQueryable<OBPA> GetOBPA()
        {
            return db.OBPA;
        }
        #endregion
        #region GET: api/AddressesBussinesPartners/{id}
        public OBPA GetOBPAById(int id)
        {
            OBPA AddressBussinesPartner = db.OBPA.Find(id);
            return AddressBussinesPartner;
        }
        #endregion
        #region POST: api/AddressesBussinesPartners
        public bool PostOBPA(OBPA AddressBussinesPartner)
        {
            try
            {
                db.OBPA.Add(AddressBussinesPartner);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
        #region PUT: api/AddressesBussinesPartners/5
        public String PutOBPA(OBPA AddressBussinesPartner)
        {
            try
            {
                db.Entry(AddressBussinesPartner).State = EntityState.Modified;
                db.SaveChanges();
                return "succes";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        #endregion
        #region EXISTS CHECKING ID
        public bool Exists(int id)
        {
            return db.OBPA.Count(e => e.Id.Equals(id)) > 0;
        }
        #endregion
        #region DISPOSE
        public void Dispose()
        {
            db.Dispose();
        }
        #endregion
    }
}