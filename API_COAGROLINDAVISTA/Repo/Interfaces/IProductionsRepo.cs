﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_COAGROLINDAVISTA.Repo.Interfaces
{
    public interface IProductionsRepo
    {
        IQueryable<OPRO> GetOPRO();
        OPRO GetOPROById(int id);
        String PutOPRO(OPRO Production);
        bool PostOPRO(OPRO Production);
        bool DeleteOPRO(int id);
        bool Exists(int id);
        void Dispose();
    }
}
