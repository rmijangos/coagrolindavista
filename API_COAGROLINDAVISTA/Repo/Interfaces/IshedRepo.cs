﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Linq;

namespace API_COAGROLINDAVISTA.Repo.Interfaces
{
    public interface IshedRepo
    {
        IQueryable<OSHE> GetOSHE();
        OSHE GetOSHEById(int id);
        String PutOSHE(OSHE Shed);
        bool PostOSHE(OSHE Shed);
        bool DeleteOSHE(int id);
        bool Exists(int id);
        void Dispose();
    }
}
