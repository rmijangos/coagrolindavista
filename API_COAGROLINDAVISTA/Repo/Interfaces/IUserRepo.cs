﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_COAGROLINDAVISTA.Repo.Interfaces
{
    public interface IUserRepo
    {
        IQueryable<OUSR> GetOUSR();
        OUSR GetOUSRById(int id);
        OUSR GetOUSRByUserAndPass(String User, String Pass);
        String PutOUSR(OUSR User);
        bool PostOUSR(OUSR User);
        bool DeleteOUSR(int id);
        bool Exists(int id);
        void Dispose();
    }
}
