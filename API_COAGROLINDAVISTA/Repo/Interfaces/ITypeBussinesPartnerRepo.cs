﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_COAGROLINDAVISTA.Repo.Interfaces
{
    public interface ITypeBussinesPartnerRepo
    {
        IQueryable<OTBP> GetOTBP();
        OTBP GetOTBPById(int id);
        String PutOTBP(OTBP TypeBussinesPartner);
        bool PostOTBP(OTBP TypeBussinesPartner);
        bool DeleteOTBP(int id);
        bool Exists(int id);
        void Dispose();
    }
}
