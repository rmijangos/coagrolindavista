﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API_COAGROLINDAVISTA.Models;

namespace API_COAGROLINDAVISTA.Repo.Interfaces
{
    public interface IGenresChickensRepo
    {
        IQueryable<OCHK> GetOCHK();
        OCHK GetOCHKById(int id);
        String PutOCHK(OCHK GenreChicken);
        bool PostOCHK(OCHK GenreChicken);
        bool DeleteOCHK(int id);
        bool Exists(int id);
        void Dispose();
    }
}
