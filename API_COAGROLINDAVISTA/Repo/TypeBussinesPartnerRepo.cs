﻿using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Repo.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace API_COAGROLINDAVISTA.Repo
{
    public class TypeBussinesPartnerRepo : ITypeBussinesPartnerRepo
    {
        #region Variables in the scope Class BussinesPartnerRepo
        private COAGROLINDAVISTAEntities db;
        #endregion
        #region Constructor DI Entities
        public TypeBussinesPartnerRepo(COAGROLINDAVISTAEntities db)
        {
            this.db = db;
        }
        #endregion
        #region DELETE: api/TypeBussinesPartner/
        public bool DeleteOTBP(int id)
        {
            OTBP TypeBussinesPartner = db.OTBP.Find(id);
            if (TypeBussinesPartner == null)
            {
                return false;
            }
            db.OTBP.Remove(TypeBussinesPartner);
            db.SaveChanges();
            return true;
        }
        #endregion
        #region GET: api/TypeBussinesPartner
        public IQueryable<OTBP> GetOTBP()
        {
            return db.OTBP;
        }
        #endregion
        #region GET: api/TypeBussinesPartner/{id}
        public OTBP GetOTBPById(int id)
        {
            OTBP TypeBussinesPartner = db.OTBP.Find(id);
            return TypeBussinesPartner;
        }
        #endregion
        #region POST: api/TypeBussinesPartner
        public bool PostOTBP(OTBP TypeBussinesPartner)
        {
            try
            {
                db.OTBP.Add(TypeBussinesPartner);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
        #region PUT: api/TypeBussinesPartner/5
        public String PutOTBP(OTBP TypeBussinesPartner)
        {
            try
            {
                db.Entry(TypeBussinesPartner).State = EntityState.Modified;
                db.SaveChanges();
                return "succes";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        #endregion
        #region EXISTS CHECKING ID
        public bool Exists(int id)
        {
            return db.OTBP.Count(e => e.IdTBP.Equals(id)) > 0;
        }
        #endregion
        #region DISPOSE
        public void Dispose()
        {
            db.Dispose();
        }
        #endregion
    }
}