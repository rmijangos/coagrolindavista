﻿var btnSignIn = document.getElementById("btnSignIn");
btnSignIn.onclick = function () {
    var user = document.getElementById("txtUser").value;
    var pass = document.getElementById("txtPassword").value;

    if (user == "") {
        alert("* User field is mandatory");
        return;
    }
    if (pass == "") {
        alert("* Pass field is mandatory");
        return;
    }


    $.get("Login/ValidateUser/?user=" + user + "&pass=" + pass, function (data) {
        if (data == 1) {
            //document.location.href ='@Url.Action("Index","Productions")';
            window.location = "/Productions/Index";
        } else {
            alert("User doesn't is allowed ");
        }
    })
}