﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Net.Http;

namespace API_MVC.Controllers
{
    public class ShedsController : Controller
    {
        #region METHODS for general use
        private OSHE GetShedByID(int id)
        {
            OSHE Shed = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP GET
                var responseTask = client.GetAsync("shelds/" + id);
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<OSHE>();
                    readTask.Wait();
                    Shed = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    Shed = new OSHE();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return Shed;
        }
        #endregion
        #region GET: Sheds - View Index
        public ActionResult Index()
        {
            IEnumerable<OSHE> Sheds = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP GET
                var responseTask = client.GetAsync("shelds");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<OSHE>>();
                    readTask.Wait();
                    Sheds = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    Sheds = Enumerable.Empty<OSHE>();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(Sheds);
        }
        #endregion
        #region GET: Sheds/Details/5 - View Details
        public ActionResult Details(int id)
        {
            return View(GetShedByID(id));
        }
        #endregion
        #region GET: Sheds/Create - Controller Create / Not implemented
        public ActionResult Create()
        {
            return View();
        }
        #endregion
        #region POST: Sheds/Create View Sheds/Create
        [HttpPost]
        public ActionResult Create(OSHE Shed)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP POST
                var postTask = client.PostAsJsonAsync<OSHE>("shelds", Shed);
                postTask.Wait();
                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(Shed);
        }
        #endregion
        #region GET: Sheds/Edit/5 - Sheds/Edit
        public ActionResult Edit(int id)
        {
            return View(GetShedByID(id));
        }
        #endregion
        #region POST: Sheds/Edit/5
        // PUT : Sheds/Edit/5
        [HttpPost]
        public ActionResult Edit(OSHE Shed)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP PUT
                var putTask = client.PutAsJsonAsync<OSHE>("shelds/", Shed);
                putTask.Wait();
                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(Shed);
        }
        #endregion
        #region GET: Sheds/Delete/5
        public ActionResult Delete(int id)
        {
            return View(GetShedByID(id));
        }
        #endregion
        #region POST: Sheds/Delete/5 - View Sheds/Delete
        [HttpPost]
        public ActionResult Delete(int id, OSHE Shed)
        {
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP POST
                var deleteTask = client.DeleteAsync("Shelds/" + id);
                deleteTask.Wait();
                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(Shed);
        }
        #endregion
    }
}
