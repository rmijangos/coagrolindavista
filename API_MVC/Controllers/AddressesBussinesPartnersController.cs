﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace API_MVC.Controllers
{
    public class AddressesBussinesPartnersController : Controller
    {
        #region METHODS for general use
        #region GetAddressesBussinesPartnerByID
        private OBPA GetAddressesBussinesPartnerByID(int id)
        {
            OBPA AddressesBussinesPartner = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP GET
                var responseTask = client.GetAsync("AddressesBussinesPartners/" + id);
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<OBPA>();
                    readTask.Wait();
                    AddressesBussinesPartner = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    AddressesBussinesPartner = new OBPA();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return AddressesBussinesPartner;
        }
        #endregion
        #region METHOD GetListBussinesPartner
        private List<OBPR> GetListBussinesPartner()
        {
            List<OBPR> OBPR = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP GET
                var responseTask = client.GetAsync("BussinesPartners");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<OBPR>>();
                    readTask.Wait();
                    OBPR = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    OBPR = new List<OBPR>();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return OBPR;
        }
        #endregion

        #endregion
        #region GET: AddressesBussinesPartners - View Index
        public ActionResult Index()
        {
            ViewBag.BussinesPartner = GetListBussinesPartner();
            IEnumerable<OBPA> AddressesBussinesPartners = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP GET
                var responseTask = client.GetAsync("AddressesBussinesPartners");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<OBPA>>();
                    readTask.Wait();
                    AddressesBussinesPartners = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    AddressesBussinesPartners = Enumerable.Empty<OBPA>();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(AddressesBussinesPartners);
        }
        #endregion
        #region GET: AddressesBussinesPartners/Details/5 - View Details
        public ActionResult Details(int id)
        {
            ViewBag.BussinesPartner = GetListBussinesPartner();
            return View(GetAddressesBussinesPartnerByID(id));
        }
        #endregion
        #region GET: AddressesBussinesPartners/Create - Controller Create / Not implemented
        public ActionResult Create()
        {
            ViewBag.BussinesPartner = GetListBussinesPartner();
            return View();
        }
        #endregion
        #region POST: AddressesBussinesPartners/Create View AddressesBussinesPartners/Create
        [HttpPost]
        public ActionResult Create(OBPA AddressesBussinesPartner)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP POST
                var postTask = client.PostAsJsonAsync<OBPA>("AddressesBussinesPartners", AddressesBussinesPartner);
                postTask.Wait();
                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(AddressesBussinesPartner);
        }
        #endregion
        #region GET: AddressesBussinesPartners/Edit/5 - AddressesBussinesPartners/Edit
        public ActionResult Edit(int id)
        {
            ViewBag.BussinesPartner = GetListBussinesPartner();
            return View(GetAddressesBussinesPartnerByID(id));
        }
        #endregion
        #region POST: AddressesBussinesPartners/Edit/5
        // PUT : AddressesBussinesPartners/Edit/5
        [HttpPost]
        public ActionResult Edit(OBPA AddressesBussinesPartner)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP PUT
                var putTask = client.PutAsJsonAsync<OBPA>("AddressesBussinesPartners/", AddressesBussinesPartner);
                putTask.Wait();
                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(AddressesBussinesPartner);
        }
        #endregion
        #region GET: AddressesBussinesPartners/Delete/5
        public ActionResult Delete(int id)
        {
            ViewBag.BussinesPartner = GetListBussinesPartner();
            return View(GetAddressesBussinesPartnerByID(id));
        }
        #endregion
        #region POST: AddressesBussinesPartners/Delete/5 - View AddressesBussinesPartners/Delete
        [HttpPost]
        public ActionResult Delete(int id, OBPA AddressesBussinesPartner)
        {
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP POST
                var deleteTask = client.DeleteAsync("AddressesBussinesPartners/" + id);
                deleteTask.Wait();
                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ViewBag.BussinesPartner = GetListBussinesPartner();
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(AddressesBussinesPartner);
        }
        #endregion
    }
}
