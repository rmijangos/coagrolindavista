﻿using API_MVC.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace API_MVC.Controllers
{
    public static class GenerJWT
    {
        public static String RequestToken(UserInfo user) {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP POST
                var postTask = client.PostAsJsonAsync<UserInfo>("LoginJWT", user);
                postTask.Wait();
                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    //System.Diagnostics.Debug.WriteLine(result.Content.ReadAsStringAsync().Result);
                    var json = result.Content.ReadAsStringAsync().Result;
                    TokenJWT token = JsonConvert.DeserializeObject<TokenJWT>(json);
                    //System.Diagnostics.Debug.WriteLine(token.Token);
                    return token.Token;
                }
            }


            return null;
        }
    }
}