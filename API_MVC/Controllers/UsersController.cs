﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace API_MVC.Controllers
{
    public class UsersController : Controller
    {
        #region METHODS for general use
        #region METHOD GetUserByID
        private OUSR GetUserByID(int id)
        {
            OUSR User = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Users/" + id);
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<OUSR>();
                    readTask.Wait();
                    User = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    User = new OUSR();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return User;
        }
        #endregion
        #region METHOD GetListUserRoles
        private List<USR1> GetListUserRoles()
        {
            List<USR1> USR1 = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP GET
                var responseTask = client.GetAsync("UsersRoles");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<USR1>>();
                    readTask.Wait();
                    USR1 = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    USR1 = new List<USR1>();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return USR1;
        }
        #endregion
        #endregion
        #region GET: Users - View Index
        public ActionResult Index()
        {
            ViewBag.UserRoles = GetListUserRoles();
            IEnumerable<OUSR> Users = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Users");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<OUSR>>();
                    readTask.Wait();
                    Users = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    Users = Enumerable.Empty<OUSR>();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(Users);
        }
        #endregion
        #region GET: Users/Details/5 - View Details
        public ActionResult Details(int id)
        {
            ViewBag.UserRoles = GetListUserRoles();
            return View(GetUserByID(id));
        }
        #endregion
        #region GET: Users/Create - Controller Create
        public ActionResult Create()
        {
            ViewBag.UserRoles = GetListUserRoles();
            return View();
        }
        #endregion
        #region POST: Users/Create View Users/Create
        [HttpPost]
        public ActionResult Create(OUSR User)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP POST
                var postTask = client.PostAsJsonAsync<OUSR>("Users", User);
                postTask.Wait();
                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ViewBag.UserRoles = GetListUserRoles();
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(User);
        }
        #endregion
        #region GET: Users/Edit/5 - Users/Edit
        public ActionResult Edit(int id)
        {
            ViewBag.UserRoles = GetListUserRoles();
            return View(GetUserByID(id));
        }
        #endregion
        #region POST: Users/Edit/5
        // PUT : Users/Edit/5
        [HttpPost]
        public ActionResult Edit(OUSR User)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP PUT
                var putTask = client.PutAsJsonAsync<OUSR>("Users/", User);
                putTask.Wait();
                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ViewBag.UserRoles = GetListUserRoles();
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(User);
        }
        #endregion
        #region GET: Users/Delete/5
        public ActionResult Delete(int id)
        {
            ViewBag.UserRoles = GetListUserRoles();
            return View(GetUserByID(id));
        }
        #endregion
        #region POST: Users/Delete/5 - View Users/Delete
        [HttpPost]
        public ActionResult Delete(int id, OUSR User)
        {
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP POST
                var deleteTask = client.DeleteAsync("Users/" + id);
                deleteTask.Wait();
                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ViewBag.UserRoles = GetListUserRoles();
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(User);
        }
        #endregion
    }
}
