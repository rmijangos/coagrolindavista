﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace API_MVC.Controllers
{
    public class GenresChickensController : Controller
    {
        #region METHODS for general use
        private OCHK GetGenresChickenByID(int id)
        {
            OCHK GenresChicken = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP GET
                var responseTask = client.GetAsync("GenresChickens/" + id);
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<OCHK>();
                    readTask.Wait();
                    GenresChicken = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    GenresChicken = new OCHK();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return GenresChicken;
        }
        #endregion
        #region GET: GenresChickens - View Index
        public ActionResult Index()
        {
            IEnumerable<OCHK> GenresChickens = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP GET
                var responseTask = client.GetAsync("GenresChickens");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<OCHK>>();
                    readTask.Wait();
                    GenresChickens = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    GenresChickens = Enumerable.Empty<OCHK>();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(GenresChickens);
        }
        #endregion
        #region GET: GenresChickens/Details/5 - View Details
        public ActionResult Details(int id)
        {
            return View(GetGenresChickenByID(id));
        }
        #endregion
        #region GET: GenresChickens/Create - Controller Create / Not implemented
        public ActionResult Create()
        {
            return View();
        }
        #endregion
        #region POST: GenresChickens/Create View GenresChickens/Create
        [HttpPost]
        public ActionResult Create(OCHK GenresChicken)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP POST
                var postTask = client.PostAsJsonAsync<OCHK>("GenresChickens", GenresChicken);
                postTask.Wait();
                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(GenresChicken);
        }
        #endregion
        #region GET: GenresChickens/Edit/5 - GenresChickens/Edit
        public ActionResult Edit(int id)
        {
            return View(GetGenresChickenByID(id));
        }
        #endregion
        #region POST: GenresChickens/Edit/5
        // PUT : GenresChickens/Edit/5
        [HttpPost]
        public ActionResult Edit(OCHK GenresChicken)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP PUT
                var putTask = client.PutAsJsonAsync<OCHK>("GenresChickens/", GenresChicken);
                putTask.Wait();
                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(GenresChicken);
        }
        #endregion
        #region GET: GenresChickens/Delete/5
        public ActionResult Delete(int id)
        {
            return View(GetGenresChickenByID(id));
        }
        #endregion
        #region POST: GenresChickens/Delete/5 - View GenresChickens/Delete
        [HttpPost]
        public ActionResult Delete(int id, OCHK GenresChicken)
        {
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP POST
                var deleteTask = client.DeleteAsync("GenresChickens/" + id);
                deleteTask.Wait();
                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(GenresChicken);
        }
        #endregion
    }
}
