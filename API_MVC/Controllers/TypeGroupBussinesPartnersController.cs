﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace API_MVC.Controllers
{
    public class TypeGroupBussinesPartnersController : Controller
    {
        #region METHODS for general use
        private OTGB GetTypeGroupBussinesPartnersByID(int id)
        {
            OTGB TypeGroupBussinesPartners = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP GET
                var responseTask = client.GetAsync("TypeGroupBussinesPartners/" + id);
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<OTGB>();
                    readTask.Wait();
                    TypeGroupBussinesPartners = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    TypeGroupBussinesPartners = new OTGB();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return TypeGroupBussinesPartners;
        }
        #endregion
        #region GET: TypeGroupBussinesPartners - View Index
        public ActionResult Index()
        {
            IEnumerable<OTGB> TypeGroupBussinesPartners = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP GET
                var responseTask = client.GetAsync("TypeGroupBussinesPartners");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<OTGB>>();
                    readTask.Wait();
                    TypeGroupBussinesPartners = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    TypeGroupBussinesPartners = Enumerable.Empty<OTGB>();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(TypeGroupBussinesPartners);
        }
        #endregion
        #region GET: TypeGroupBussinesPartners/Details/5 - View Details
        public ActionResult Details(int id)
        {
            return View(GetTypeGroupBussinesPartnersByID(id));
        }
        #endregion
        #region GET: TypeGroupBussinesPartners/Create - Controller Create / Not implemented
        public ActionResult Create()
        {
            return View();
        }
        #endregion
        #region POST: TypeGroupBussinesPartners/Create View TypeGroupBussinesPartners/Create
        [HttpPost]
        public ActionResult Create(OTGB TypeGroupBussinesPartners)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP POST
                var postTask = client.PostAsJsonAsync<OTGB>("TypeGroupBussinesPartners", TypeGroupBussinesPartners);
                postTask.Wait();
                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(TypeGroupBussinesPartners);
        }
        #endregion
        #region GET: TypeGroupBussinesPartners/Edit/5 - TypeGroupBussinesPartners/Edit
        public ActionResult Edit(int id)
        {
            return View(GetTypeGroupBussinesPartnersByID(id));
        }
        #endregion
        #region POST: TypeGroupBussinesPartners/Edit/5
        // PUT : TypeGroupBussinesPartners/Edit/5
        [HttpPost]
        public ActionResult Edit(OTGB TypeGroupBussinesPartners)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP PUT
                var putTask = client.PutAsJsonAsync<OTGB>("TypeGroupBussinesPartners/", TypeGroupBussinesPartners);
                putTask.Wait();
                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(TypeGroupBussinesPartners);
        }
        #endregion
        #region GET: TypeGroupBussinesPartners/Delete/5
        public ActionResult Delete(int id)
        {
            return View(GetTypeGroupBussinesPartnersByID(id));
        }
        #endregion
        #region POST: TypeGroupBussinesPartners/Delete/5 - View TypeGroupBussinesPartners/Delete
        [HttpPost]
        public ActionResult Delete(int id, OTGB TypeGroupBussinesPartners)
        {
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP POST
                var deleteTask = client.DeleteAsync("TypeGroupBussinesPartners/" + id);
                deleteTask.Wait();
                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(TypeGroupBussinesPartners);
        }
        #endregion
    }
}
