﻿using API_COAGROLINDAVISTA.Models;
using API_MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace API_MVC.Controllers
{
    public class ProductionsController : Controller
    {
        #region METHODS for general use
        #region GetProductionByID
        private OPRO GetProductionByID(int id)
        {
            OPRO Production = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Productions/" + id);
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<OPRO>();
                    readTask.Wait();
                    Production = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    Production = new OPRO();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return Production;
        }
        #endregion
        #region METHOD GetListGenresChickens
        private List<OCHK> GetListGenresChickens()
        {
            List<OCHK> OCHK = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP GET
                var responseTask = client.GetAsync("GenresChickens");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<OCHK>>();
                    readTask.Wait();
                    OCHK = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    OCHK = new List<OCHK>();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return OCHK;
        }
        #endregion
        #region METHOD GetListSheds
        private List<OSHE> GetListSheds()
        {
            List<OSHE> OSHE = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Shelds");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<OSHE>>();
                    readTask.Wait();
                    OSHE = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    OSHE = new List<OSHE>();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return OSHE;
        }
        #endregion
        #region METHOD GetListBusinessPartner
        private List<OBPR> GetListBusinessPartner()
        {
            List<OBPR> OBPR = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP GET
                var responseTask = client.GetAsync("BussinesPartners");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<OBPR>>();
                    readTask.Wait();
                    OBPR = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    OBPR = new List<OBPR>();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return OBPR;
        }
        #endregion
        #endregion
        #region GET: Productions - View Index
        public ActionResult Index()
        {
            OUSR PropertiesUser = (OUSR)Session["UserProperties"];
            ViewBag.UserName = PropertiesUser.UserName;
            ViewBag.ListGenresChickens = GetListGenresChickens();
            ViewBag.ListSheds = GetListSheds();
            ViewBag.ListBusinessPartner = GetListBusinessPartner();

            IEnumerable<OPRO> Productions = null;
            
            UserInfo Credentials = new UserInfo();
            Credentials.User = PropertiesUser.NickName;
            Credentials.Password = PropertiesUser.UserPassword;
            var tokenSended = GenerJWT.RequestToken(Credentials);
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenSended);
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Productions");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<OPRO>>();
                    readTask.Wait();
                    Productions = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    Productions = Enumerable.Empty<OPRO>();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(Productions);
        }

        private void RequestToken()
        {
            throw new NotImplementedException();
        }
        #endregion
        #region GET: Productions/Details/5 - View Details
        public ActionResult Details(int id)
        {
            ViewBag.ListGenresChickens = GetListGenresChickens();
            ViewBag.ListSheds = GetListSheds();
            ViewBag.ListBusinessPartner = GetListBusinessPartner();
            return View(GetProductionByID(id));
        }
        #endregion
        #region GET: Productions/Create - Controller Create
        public ActionResult Create()
        {
            ViewBag.ListGenresChickens = GetListGenresChickens();
            ViewBag.ListSheds = GetListSheds();
            ViewBag.ListBusinessPartner = GetListBusinessPartner();
            return View();
        }
        #endregion
        #region POST: Productions/Create View Productions/Create
        [HttpPost]
        public ActionResult Create(OPRO Production)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP POST
                var postTask = client.PostAsJsonAsync<OPRO>("Productions", Production);
                postTask.Wait();
                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ViewBag.ListGenresChickens = GetListGenresChickens();
            ViewBag.ListSheds = GetListSheds();
            ViewBag.ListBusinessPartner = GetListBusinessPartner();
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(Production);
        }
        #endregion
        #region GET: Productions/Edit/5 - Productions/Edit
        public ActionResult Edit(int id)
        {
            ViewBag.ListGenresChickens = GetListGenresChickens();
            ViewBag.ListSheds = GetListSheds();
            ViewBag.ListBusinessPartner = GetListBusinessPartner();
            return View(GetProductionByID(id));
        }
        #endregion
        #region POST: Productions/Edit/5
        // PUT : Productions/Edit/5
        [HttpPost]
        public ActionResult Edit(OPRO Production)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP PUT
                var putTask = client.PutAsJsonAsync<OPRO>("Productions/", Production);
                putTask.Wait();
                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ViewBag.ListGenresChickens = GetListGenresChickens();
            ViewBag.ListSheds = GetListSheds();
            ViewBag.ListBusinessPartner = GetListBusinessPartner();
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(Production);
        }
        #endregion
        #region GET: Productions/Delete/5
        public ActionResult Delete(int id)
        {
            ViewBag.ListGenresChickens = GetListGenresChickens();
            ViewBag.ListSheds = GetListSheds();
            ViewBag.ListBusinessPartner = GetListBusinessPartner();
            return View(GetProductionByID(id));
        }
        #endregion
        #region POST: Productions/Delete/5 - View Productions/Delete
        [HttpPost]
        public ActionResult Delete(int id, OPRO Production)
        {
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP POST
                var deleteTask = client.DeleteAsync("Productions/" + id);
                deleteTask.Wait();
                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ViewBag.ListGenresChickens = GetListGenresChickens();
            ViewBag.ListSheds = GetListSheds();
            ViewBag.ListBusinessPartner = GetListBusinessPartner();
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(Production);
        }
        #endregion
    }
}
