﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace API_MVC.Controllers
{
    public class UsersRolesController : Controller
    {
        #region METHODS for general use
        private USR1 GetShedByID(int id)
        {
            USR1 Shed = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP GET
                var responseTask = client.GetAsync("UsersRoles/" + id);
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<USR1>();
                    readTask.Wait();
                    Shed = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    Shed = new USR1();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return Shed;
        }
        #endregion
        #region GET: UsersRoles - View Index
        public ActionResult Index()
        {
            IEnumerable<USR1> UsersRoles = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP GET
                var responseTask = client.GetAsync("UsersRoles");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<USR1>>();
                    readTask.Wait();
                    UsersRoles = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    UsersRoles = Enumerable.Empty<USR1>();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(UsersRoles);
        }
        #endregion
        #region GET: UsersRoles/Details/5 - View Details
        public ActionResult Details(int id)
        {
            return View(GetShedByID(id));
        }
        #endregion
        #region GET: UsersRoles/Create - Controller Create / Not implemented
        public ActionResult Create()
        {
            return View();
        }
        #endregion
        #region POST: UsersRoles/Create View UsersRoles/Create
        [HttpPost]
        public ActionResult Create(USR1 Shed)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP POST
                var postTask = client.PostAsJsonAsync<USR1>("UsersRoles", Shed);
                postTask.Wait();
                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(Shed);
        }
        #endregion
        #region GET: UsersRoles/Edit/5 - UsersRoles/Edit
        public ActionResult Edit(int id)
        {
            return View(GetShedByID(id));
        }
        #endregion
        #region POST: UsersRoles/Edit/5
        // PUT : UsersRoles/Edit/5
        [HttpPost]
        public ActionResult Edit(USR1 Shed)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP PUT
                var putTask = client.PutAsJsonAsync<USR1>("UsersRoles/", Shed);
                putTask.Wait();
                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(Shed);
        }
        #endregion
        #region GET: UsersRoles/Delete/5
        public ActionResult Delete(int id)
        {
            return View(GetShedByID(id));
        }
        #endregion
        #region POST: UsersRoles/Delete/5 - View UsersRoles/Delete
        [HttpPost]
        public ActionResult Delete(int id, USR1 Shed)
        {
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP POST
                var deleteTask = client.DeleteAsync("UsersRoles/" + id);
                deleteTask.Wait();
                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(Shed);
        }
        #endregion
    }
}
