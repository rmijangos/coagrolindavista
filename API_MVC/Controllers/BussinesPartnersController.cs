﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace API_MVC.Controllers
{
    public class BussinesPartnersController : Controller
    {
        #region METHODS for general use
        #region METHOD GetBussinesPartnerID
        private OBPR GetBussinesPartnerID(String id)
        {
            OBPR BussinesPartner = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP GET
                var responseTask = client.GetAsync("BussinesPartners/" + id);
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<OBPR>();
                    readTask.Wait();
                    BussinesPartner = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    BussinesPartner = new OBPR();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return BussinesPartner;
        }
        #endregion
        #region METHOD GetListGroupBussinesPartner
        private List<OTGB> GetListGroupBussinesPartner()
        {
            List<OTGB> OTGB = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP GET
                var responseTask = client.GetAsync("TypeGroupBussinesPartners");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<OTGB>>();
                    readTask.Wait();
                    OTGB = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    OTGB = new List<OTGB>();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return OTGB;
        }
        #endregion
        #region METHOD GetListTypeGroupBussinesPartner
        private List<OTBP> GetListTypeGroupBussinesPartner()
        {
            List<OTBP> OTBP = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP GET
                var responseTask = client.GetAsync("TypeBussinesPartners");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<OTBP>>();
                    readTask.Wait();
                    OTBP = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    OTBP = new List<OTBP>();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return OTBP;
        }
        #endregion
        #endregion
        #region GET: BussinesPartners - View Index
        public ActionResult Index()
        {
            ViewBag.GroupBussinesPartner = GetListGroupBussinesPartner();
            ViewBag.TypeGroupBussinesPartner = GetListTypeGroupBussinesPartner();
            IEnumerable<OBPR> BussinesPartners = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP GET
                var responseTask = client.GetAsync("BussinesPartners");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<OBPR>>();
                    readTask.Wait();
                    BussinesPartners = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    BussinesPartners = Enumerable.Empty<OBPR>();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(BussinesPartners);
        }
        #endregion
        #region GET: BussinesPartners/Details/5 - View Details
        public ActionResult Details(String id)
        {
            ViewBag.GroupBussinesPartner = GetListGroupBussinesPartner();
            ViewBag.TypeGroupBussinesPartner = GetListTypeGroupBussinesPartner();
            return View(GetBussinesPartnerID(id));
        }
        #endregion
        #region GET: BussinesPartners/Create - Controller Create
        public ActionResult Create()
        {
            ViewBag.GroupBussinesPartner = GetListGroupBussinesPartner();
            ViewBag.TypeGroupBussinesPartner = GetListTypeGroupBussinesPartner();
            return View();
        }
        #endregion
        #region POST: BussinesPartners/Create View BussinesPartners/Create
        [HttpPost]
        public ActionResult Create(OBPR BussinesPartner)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP POST
                var postTask = client.PostAsJsonAsync<OBPR>("BussinesPartners", BussinesPartner);
                postTask.Wait();
                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ViewBag.GroupBussinesPartner = GetListGroupBussinesPartner();
            ViewBag.TypeGroupBussinesPartner = GetListTypeGroupBussinesPartner();
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(BussinesPartner);
        }
        #endregion
        #region GET: BussinesPartners/Edit/5 - BussinesPartners/Edit
        public ActionResult Edit(String id)
        {
            ViewBag.GroupBussinesPartner = GetListGroupBussinesPartner();
            ViewBag.TypeGroupBussinesPartner = GetListTypeGroupBussinesPartner();
            return View(GetBussinesPartnerID(id));
        }
        #endregion
        #region POST: BussinesPartners/Edit/5
        // PUT : BussinesPartners/Edit/5
        [HttpPost]
        public ActionResult Edit(OBPR BussinesPartner)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP PUT
                var putTask = client.PutAsJsonAsync<OBPR>("BussinesPartners/", BussinesPartner);
                putTask.Wait();
                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(BussinesPartner);
        }
        #endregion
        #region GET: BussinesPartners/Delete/5
        public ActionResult Delete(String id)
        {
            ViewBag.GroupBussinesPartner = GetListGroupBussinesPartner();
            ViewBag.TypeGroupBussinesPartner = GetListTypeGroupBussinesPartner();
            return View(GetBussinesPartnerID(id));
        }
        #endregion
        #region POST: BussinesPartners/Delete/5 - View BussinesPartners/Delete
        [HttpPost]
        public ActionResult Delete(int id, OBPR BussinesPartner)
        {
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP POST
                var deleteTask = client.DeleteAsync("BussinesPartners/" + id);
                deleteTask.Wait();
                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(BussinesPartner);
        }
        #endregion
    }
}
