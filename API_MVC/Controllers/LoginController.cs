﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace API_MVC.Controllers
{
    public class LoginController : Controller
    {
        #region VIEW GET Login
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region Methods Controller
        #region ValidateUser to Log in
        public int ValidateUser(String user, String pass)
        {
            System.Diagnostics.Debug.WriteLine("Usuario: " + user);
            System.Diagnostics.Debug.WriteLine("Contra " + pass);
            OUSR OUser = null;
            int response = 0;
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("https://localhost:44377/api/");
                    //HTTP GET
                    var responseTask = client.GetAsync("Users/UserLogIn/" + user + "/" + pass);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<OUSR>();
                        readTask.Wait();
                        OUser = readTask.Result;
                        if (OUser != null)
                        {
                            Session["UserProperties"] = OUser;
                            response = 1;
                        }
                        else { response = 0; }

                    }
                    else //web api sent error response 
                    {
                        //log response status here..
                        OUser = new OUSR();
                        response = 0;
                        ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                    }
                }
            }
            catch (Exception)
            {
                //0 Fail response
                response = 0;
            }
            return response;
        }
        #endregion
        #region SignOut
        public ActionResult SignOut()
        {
            return RedirectToAction("Index");
        }
        #endregion
        #endregion
    }
}