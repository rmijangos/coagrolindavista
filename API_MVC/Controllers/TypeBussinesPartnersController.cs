﻿using API_COAGROLINDAVISTA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace API_MVC.Controllers
{
    public class TypeBussinesPartnersController : Controller
    {
        #region METHODS for general use
        private OTBP GetTypeBussinesPartnerByID(int id)
        {
            OTBP TypeBussinesPartner = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP GET
                var responseTask = client.GetAsync("TypeBussinesPartners/" + id);
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<OTBP>();
                    readTask.Wait();
                    TypeBussinesPartner = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    TypeBussinesPartner = new OTBP();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return TypeBussinesPartner;
        }
        #endregion
        #region GET: TypeBussinesPartners - View Index
        public ActionResult Index()
        {
            IEnumerable<OTBP> TypeBussinesPartners = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP GET
                var responseTask = client.GetAsync("TypeBussinesPartners");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<OTBP>>();
                    readTask.Wait();
                    TypeBussinesPartners = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    TypeBussinesPartners = Enumerable.Empty<OTBP>();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(TypeBussinesPartners);
        }
        #endregion
        #region GET: TypeBussinesPartners/Details/5 - View Details
        public ActionResult Details(int id)
        {
            return View(GetTypeBussinesPartnerByID(id));
        }
        #endregion
        #region GET: TypeBussinesPartners/Create - Controller Create / Not implemented
        public ActionResult Create()
        {
            return View();
        }
        #endregion
        #region POST: TypeBussinesPartners/Create View TypeBussinesPartners/Create
        [HttpPost]
        public ActionResult Create(OTBP TypeBussinesPartner)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP POST
                var postTask = client.PostAsJsonAsync<OTBP>("TypeBussinesPartners", TypeBussinesPartner);
                postTask.Wait();
                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(TypeBussinesPartner);
        }
        #endregion
        #region GET: TypeBussinesPartners/Edit/5 - TypeBussinesPartners/Edit
        public ActionResult Edit(int id)
        {
            return View(GetTypeBussinesPartnerByID(id));
        }
        #endregion
        #region POST: TypeBussinesPartners/Edit/5
        // PUT : TypeBussinesPartners/Edit/5
        [HttpPost]
        public ActionResult Edit(OTBP TypeBussinesPartner)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP PUT
                var putTask = client.PutAsJsonAsync<OTBP>("TypeBussinesPartners/", TypeBussinesPartner);
                putTask.Wait();
                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(TypeBussinesPartner);
        }
        #endregion
        #region GET: TypeBussinesPartners/Delete/5
        public ActionResult Delete(int id)
        {
            return View(GetTypeBussinesPartnerByID(id));
        }
        #endregion
        #region POST: TypeBussinesPartners/Delete/5 - View TypeBussinesPartners/Delete
        [HttpPost]
        public ActionResult Delete(int id, OTBP TypeBussinesPartner)
        {
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri("https://localhost:44377/api/");
                //HTTP POST
                var deleteTask = client.DeleteAsync("TypeBussinesPartners/" + id);
                deleteTask.Wait();
                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            return View(TypeBussinesPartner);
        }
        #endregion
    }
}
