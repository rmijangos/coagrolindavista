﻿using System;
using API_COAGROLINDAVISTA.Controllers;
using API_COAGROLINDAVISTA.Models;
using API_COAGROLINDAVISTA.Repo.Interfaces;
using API_COAGROLINDAVISTA.Services;
using API_COAGROLINDAVISTA.Services.interfaces;
using Moq;
using NUnit.Framework;

namespace API_COAGROLINDAVISTA_UNIT_TESTS
{
    public class LoginJWTControllerTests
    {
        Mock<IGenerateJWTRepo> LoginRepoMock;
        GenerateJWTService ServiceJWT;

        [SetUp]
        public void setup()
        {
            LoginRepoMock = new Mock<IGenerateJWTRepo>();
            ServiceJWT = new GenerateJWTService(LoginRepoMock.Object);
        }

        [TestCase("jason.mijangos", "123456")]
        public void ValidateCredentials(String User, String Password)
        {
            //Arrange
            String LastName = "Mijangos";
            LoginRepoMock.Setup(x => x.GetOUSRByUserAndPass(User, Password))
                .Returns(
                                 new OUSR()
                                 {
                                     IdUser = 1,
                                     NickName = "jason.mijangos",
                                     UserName = "Jason",
                                     UserLastName = LastName,
                                     UserPassword = "123456",
                                     IdRole = 1
                                 });
            //Act
            var user = ServiceJWT.AuthUser(User, Password);
            //Assert
            Assert.AreEqual(user.LastName, LastName);
        }

        [TestCase("jason.mijangos", "123456")]
        public void ValidateCredentialsNull(String User, String Password)
        {
            //Arrange
            LoginRepoMock.Setup(x => x.GetOUSRByUserAndPass(User, Password))
                .Returns((OUSR)null);
            //Act
            var user = ServiceJWT.AuthUser(User, Password);
            //Assert
            Assert.IsNull(user);
        }
    }
}
